package ph.com.truemoney.topup.configs;

//import org.springframework.amqp.core.AmqpAdmin;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.connection.ConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitAdmin;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ph.com.truemoney.base.config.BaseInitializerConfig;
import ph.com.truemoney.logger.aspect.LoggerAspect;
import ph.com.truemoney.logger.client.LoggerApiClient;

import javax.annotation.PostConstruct;

@Configuration
@Import( { BaseInitializerConfig.class, LoggerAspect.class } )
public class TopupConfig {
    @Value( "${constants.logging.uri}" )
    private String loggingUri;

    @Value( "${logger.serviceUri}" )
    private String serviceUri;

//    @Value( "${spring.rabbitmq.host}" )
//    private String host;

    /**
     * Init.
     */
    @PostConstruct
    public void init( ) {
        LoggerApiClient.setLoggerUri( loggingUri );
        LoggerApiClient.setServiceUri( serviceUri );
    }

//    @Bean
//    public ConnectionFactory connectionFactory( ) {
//        return new CachingConnectionFactory( host );
//    }
//
//    @Bean
//    public AmqpAdmin amqpAdmin( ) {
//        return new RabbitAdmin( connectionFactory( ) );
//    }
//
//    @Bean
//    public RabbitTemplate rabbitTemplate( ) {
//        return new RabbitTemplate( connectionFactory( ) );
//    }

}
