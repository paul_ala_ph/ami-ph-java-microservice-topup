package ph.com.truemoney.topup.constants;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "constants.topup")
public class TopupConstants {

    public static Wallets WALLETS;
    public static Uris URIS;
    public static Addrs ADDRS;
    public static TransTypeId TRANS_TYPE_ID;
    public static ServiceCode SERVICE_CODE;
    public static ServiceProvider SERVICE_PROVIDER;
    public static TransactionType TRANSACTION_TYPE;

    public static class ServiceProvider {

        public static Integer LOADCENTRAL;
        public static Integer ECPAY;
        public static Integer BAYADCENTER;
        public static Integer NETPLAY;
        public static Integer ILOILOSALES;
        public static Integer CPC;
        public static Integer MALAYAN;
        public static Integer TOPPSDIGITALSERVICES;
        public static Integer WORLDREMIT;
        public static Integer RDPAWNSHOP;
        public static Integer PAYNAMICS;
        public static Integer REMITBOX;
        public static Integer SECURITYBANK;
        public static Integer BANKCOMMERCE;
        public static Integer COLLECTOR;
        public static Integer HOMECREDIT;
        public static Integer FWD;
        public static Integer GLOBE;
        public static Integer LBCPADALA;
        public static Integer UNIONBANK;

        public void setLOADCENTRAL(Integer LOADCENTRAL) {
            ServiceProvider.LOADCENTRAL = LOADCENTRAL;
        }
        public void setECPAY(Integer ECPAY) {
            ServiceProvider.ECPAY = ECPAY;
        }
        public void setBAYADCENTER(Integer BAYADCENTER) {
            ServiceProvider.BAYADCENTER = BAYADCENTER;
        }
        public void setNETPLAY(Integer NETPLAY) {
            ServiceProvider.NETPLAY = NETPLAY;
        }
        public void setILOILOSALES(Integer ILOILOSALES) {
            ServiceProvider.ILOILOSALES = ILOILOSALES;
        }
        public void setCPC(Integer CPC) {
            ServiceProvider.CPC = CPC;
        }
        public void setMALAYAN(Integer MALAYAN) {
            ServiceProvider.MALAYAN = MALAYAN;
        }
        public void setTOPPSDIGITALSERVICES(Integer TOPPSDIGITALSERVICES) { ServiceProvider.TOPPSDIGITALSERVICES = TOPPSDIGITALSERVICES; }
        public void setWORLDREMIT(Integer WORLDREMIT) {
            ServiceProvider.WORLDREMIT = WORLDREMIT;
        }
        public void setRDPAWNSHOP(Integer RDPAWNSHOP) {
            ServiceProvider.RDPAWNSHOP = RDPAWNSHOP;
        }
        public void setPAYNAMICS(Integer PAYNAMICS) {
            ServiceProvider.PAYNAMICS = PAYNAMICS;
        }
        public void setREMITBOX(Integer REMITBOX) {
            ServiceProvider.REMITBOX = REMITBOX;
        }
        public void setSECURITYBANK(Integer SECURITYBANK) {
            ServiceProvider.SECURITYBANK = SECURITYBANK;
        }
        public void setBANKCOMMERCE(Integer BANKCOMMERCE) {
            ServiceProvider.BANKCOMMERCE = BANKCOMMERCE;
        }
        public void setCOLLECTOR(Integer COLLECTOR) {
            ServiceProvider.COLLECTOR = COLLECTOR;
        }
        public void setHOMECREDIT(Integer HOMECREDIT) {
            ServiceProvider.HOMECREDIT = HOMECREDIT;
        }
        public void setFWD(Integer FWD) {
            ServiceProvider.FWD = FWD;
        }
        public void setGLOBE(Integer GLOBE) {
            ServiceProvider.GLOBE = GLOBE;
        }
        public void setLBCPADALA(Integer LBCPADALA) {
            ServiceProvider.LBCPADALA = LBCPADALA;
        }
        public void setUNIONBANK(Integer UNIONBANK) {
            ServiceProvider.UNIONBANK = UNIONBANK;
        }

    }

    public static class Uris {

        public static String GETBRANCHINFO;
        public static String GETCOUNTTRANSACTION;
        public static String GETBACKLOG;
        public static String GETLOGTRANSACTION;
        public static String GETBLACKLISTMOBILENUMBER;
        public static String GETWALLETID;
        public static String GETVALIDATEDUPLICATETRANSACTION;
        public static String POSTUPDATETRANSACTIONSTATUS;
        public static String GETITEMNUMBERBYKITID;
        public static String GETCARDNUMBER;
        public static String GETCOLLECTORACCOUNTDETAILS;
        public static String GETCOLLECTORSERVICEDETAILSBYSERVICEID;
        public static String GETTRANSACTIONINQUIRYDETAILS;
        public static String GETWALLETIDBYKITID;
        public static String POSTCOMPLETEBILLING;
        public static String POSTCOMPLETEBILLINGV2;
        public static String POSTCOMPLETEBILLSPAYMENTV2;
        public static String POSTINITIATEBILLINGV2;
        public static String POSTINITIATEBILLSPAYMENTV2;
        public static String POSTINITIATETOPUPEXTERNAL;
        public static String POSTINITIATETOPUPEDC;
        public static String POSTCOMPLETETOPUPEXTERNAL;
        public static String POSTCOMPLETETOPUPEDC;
        public static String POSTINSERTSERVICETRANSACTION;
        public static String POSTPROCESSTRANSACTION;
        public static String POSTNOTIFSEND;

        public void setGETBRANCHINFO(String GETBRANCHINFO) {
            Uris.GETBRANCHINFO = GETBRANCHINFO;
        }

        public static String POSTSPIPOST;
        public static String POSTSPIVALIDATE;
        public static String POSTUPDATESERVICETRANSACTION;

        public void setGETCOLLECTORSERVICEDETAILSBYSERVICEID(String GETCOLLECTORSERVICEDETAILSBYSERVICEID) { Uris.GETCOLLECTORSERVICEDETAILSBYSERVICEID = GETCOLLECTORSERVICEDETAILSBYSERVICEID; }
        public void setGETCOLLECTORACCOUNTDETAILS(String GETCOLLECTORACCOUNTDETAILS) { Uris.GETCOLLECTORACCOUNTDETAILS = GETCOLLECTORACCOUNTDETAILS; }
        public void setGETLOGTRANSACTION(String GETLOGTRANSACTION) {
            Uris.GETLOGTRANSACTION = GETLOGTRANSACTION;
        }
        public void setGETBACKLOG(String GETBACKLOG) {
            Uris.GETBACKLOG = GETBACKLOG;
        }
        public void setGETCOUNTTRANSACTION(String GETCOUNTTRANSACTION) { Uris.GETCOUNTTRANSACTION = GETCOUNTTRANSACTION; }
        public void setGETBLACKLISTMOBILENUMBER(String GETBLACKLISTMOBILENUMBER) { Uris.GETBLACKLISTMOBILENUMBER = GETBLACKLISTMOBILENUMBER; }
        public void setGETWALLETID(String GETWALLETID) {
            Uris.GETWALLETID = GETWALLETID;
        }
        public void setGETVALIDATEDUPLICATETRANSACTION(String GETVALIDATEDUPLICATETRANSACTION) { Uris.GETVALIDATEDUPLICATETRANSACTION = GETVALIDATEDUPLICATETRANSACTION; }
        public void setPOSTUPDATETRANSACTIONSTATUS(String POSTUPDATETRANSACTIONSTATUS) { Uris.POSTUPDATETRANSACTIONSTATUS = POSTUPDATETRANSACTIONSTATUS; }
        public void setGETCARDNUMBER(String GETCARDNUMBER) {
            Uris.GETCARDNUMBER = GETCARDNUMBER;
        }
        public void setGETITEMNUMBERBYKITID(String GETITEMNUMBERBYKITID) { Uris.GETITEMNUMBERBYKITID = GETITEMNUMBERBYKITID; }
        public void setGETWALLETIDBYKITID(String GETWALLETIDBYKITID) {
            Uris.GETWALLETIDBYKITID = GETWALLETIDBYKITID;
        }
        public void setGETTRANSACTIONINQUIRYDETAILS(String GETTRANSACTIONINQUIRYDETAILS) { Uris.GETTRANSACTIONINQUIRYDETAILS = GETTRANSACTIONINQUIRYDETAILS; }
        public void setPOSTCOMPLETEBILLING(String POSTCOMPLETEBILLING) { Uris.POSTCOMPLETEBILLING = POSTCOMPLETEBILLING; }
        public void setPOSTCOMPLETEBILLSPAYMENTV2(String POSTCOMPLETEBILLSPAYMENTV2) { Uris.POSTCOMPLETEBILLSPAYMENTV2 = POSTCOMPLETEBILLSPAYMENTV2; }
        public void setPOSTINITIATEBILLSPAYMENTV2(String POSTINITIATEBILLSPAYMENTV2) { Uris.POSTINITIATEBILLSPAYMENTV2 = POSTINITIATEBILLSPAYMENTV2; }
        public void setPOSTINITIATETOPUPEXTERNAL(String POSTINITIATETOPUPEXTERNAL) { Uris.POSTINITIATETOPUPEXTERNAL = POSTINITIATETOPUPEXTERNAL; }
        public void setPOSTINITIATETOPUPEDC(String POSTINITIATETOPUPEDC) { Uris.POSTINITIATETOPUPEDC = POSTINITIATETOPUPEDC; }
        public void setPOSTCOMPLETETOPUPEXTERNAL(String POSTCOMPLETETOPUPEXTERNAL) { Uris.POSTCOMPLETETOPUPEXTERNAL = POSTCOMPLETETOPUPEXTERNAL; }
        public void setPOSTCOMPLETETOPUPEDC(String POSTCOMPLETETOPUPEDC) { Uris.POSTCOMPLETETOPUPEDC = POSTCOMPLETETOPUPEDC; }
        public void setPOSTINITIATEBILLINGV2(String POSTINITIATEBILLINGV2) { Uris.POSTINITIATEBILLINGV2 = POSTINITIATEBILLINGV2; }
        public void setPOSTCOMPLETEBILLINGV2(String POSTCOMPLETEBILLINGV2) { Uris.POSTCOMPLETEBILLINGV2 = POSTCOMPLETEBILLINGV2; }
        public void setPOSTINSERTSERVICETRANSACTION(String POSTINSERTSERVICETRANSACTION) { Uris.POSTINSERTSERVICETRANSACTION = POSTINSERTSERVICETRANSACTION; }
        public void setPOSTUPDATESERVICETRANSACTION(String POSTUPDATESERVICETRANSACTION) { Uris.POSTUPDATESERVICETRANSACTION = POSTUPDATESERVICETRANSACTION; }
        public void setPOSTSPIVALIDATE(String POSTSPIVALIDATE) {
            Uris.POSTSPIVALIDATE = POSTSPIVALIDATE;
        }
        public void setPOSTSPIPOST(String POSTSPIPOST) {
            Uris.POSTSPIPOST = POSTSPIPOST;
        }

        public void setPOSTPROCESSTRANSACTION(String POSTPROCESSTRANSACTION) {
            Uris.POSTPROCESSTRANSACTION = POSTPROCESSTRANSACTION;
        }
        public void setPOSTNOTIFSEND(String POSTNOTIFSEND) {
            Uris.POSTNOTIFSEND = POSTNOTIFSEND;
        }
    }


    public static class Addrs {
        public static String COREADDR;
        public static String SPIADDR;
        public static String NOTIFADDR;

        public void setCOREADDR(String COREADDR) {
            Addrs.COREADDR = COREADDR;
        }
        public void setSPIADDR(String SPIADDR) {
            Addrs.SPIADDR = SPIADDR;
        }

        public void setNOTIFADDR(String NOTIFADDR) {
            Addrs.NOTIFADDR = NOTIFADDR;
        }
    }


    public static class ServiceCode {
        public static String CPC;
        public static String ILOILOSALES;
        public static String MERALCO;
        public static String NBI;
        public static String PRC;
        public static String SKYCABLE;
        public static String INEC;
        public static String LAZADA;
        public static String SSS_SSID;
        public static String SSS_PRN;
        public static String HOME_CREDIT;
        public static String PHILHEALTH;
        public static String LBCPADALA;
        public static String RDPAWNSHOP;
        public static String LTO;
        public static String PILI_WATER;

        public void setCPC(String CPC) {
            ServiceCode.CPC = CPC;
        }
        public void setILOILOSALES(String ILOILOSALES) {
            ServiceCode.ILOILOSALES = ILOILOSALES;
        }
        public void setMERALCO(String MERALCO) {
            ServiceCode.MERALCO = MERALCO;
        }
        public void setNBI(String NBI) {
            ServiceCode.NBI = NBI;
        }
        public void setPRC(String PRC) {
            ServiceCode.PRC = PRC;
        }
        public void setSKYCABLE(String SKYCABLE) {
            ServiceCode.SKYCABLE = SKYCABLE;
        }
        public void setINEC(String INEC) {
            ServiceCode.INEC = INEC;
        }
        public void setLAZADA(String LAZADA) {
            ServiceCode.LAZADA = LAZADA;
        }
        public void setSssSsid(String sssSsid) {
            SSS_SSID = sssSsid;
        }
        public void setSssPrn(String sssPrn) {
            SSS_PRN = sssPrn;
        }
        public void setHomeCredit(String homeCredit) {
            HOME_CREDIT = homeCredit;
        }
        public void setPHILHEALTH(String PHILHEALTH) {
            ServiceCode.PHILHEALTH = PHILHEALTH;
        }
        public void setLBCPADALA(String LBCPADALA) {
            ServiceCode.LBCPADALA = LBCPADALA;
        }
        public void setRDPAWNSHOP(String RDPAWNSHOP) {
            ServiceCode.RDPAWNSHOP = RDPAWNSHOP;
        }
        public void setLTO(String LTO) {
            ServiceCode.LTO = LTO;
        }
        public void setPiliWater(String piliWater) {
            PILI_WATER = piliWater;
        }
    }

    public static class Wallets {
        public static Long ILOILOSALES;
        public static Long CPC;
        public static Long PALOAD;
        public static Long IFLIX;

        public void setILOILOSALES(Long ILOILOSALES) {
            Wallets.ILOILOSALES = ILOILOSALES;
        }
        public void setCPC(Long CPC) {
            Wallets.CPC = CPC;
        }
        public void setPALOAD(Long PALOAD) {Wallets.PALOAD = PALOAD;}
        public void setIFLIX(Long IFLIX) {Wallets.IFLIX = IFLIX;}
    }

    public static class TransTypeId {
        public static Integer ILOILO;
        public static Integer CPC;

        public void setILOILO(Integer ILOILO) {
            TransTypeId.ILOILO = ILOILO;
        }
        public void setCPC(Integer CPC) {
            TransTypeId.CPC = CPC;
        }
    }

    public static class TransactionType {
        public static Integer TTYPE_INITIATE_BILLSPAY;
        public static Integer TTYPE_COMPLETE_BILLSPAY;
        public static Integer INITIATE_BILLSPAY_T;
        public static Integer COMPLETE_BILLSPAY_T;
        public static Integer INITIATE_TOPUP_T;
        public static Integer COMPLETE_TOPUP_T;
        public static Integer TOPUP_REWARD_BILLING;
        public static Integer TOPUP_REWARD_RECEIVER;
        public static Integer TOPUP_REWARD_SENDER;
        public static Integer INITIATE_TOPUP_IFLIX;
        public static Integer COMPLETE_TOPUP_IFLIX;
        public static Integer INITIATE_TOPUP;
        public static Integer COMPLETE_TOPUP;



        public void setTtypeInitiateBillspay(Integer ttypeInitiateBillspay) { TTYPE_INITIATE_BILLSPAY = ttypeInitiateBillspay; }
        public void setTtypeCompleteBillspay(Integer ttypeCompleteBillspay) { TTYPE_COMPLETE_BILLSPAY = ttypeCompleteBillspay; }
        public void setInitiateBillspayT(Integer initiateBillspayT) {
            INITIATE_BILLSPAY_T = initiateBillspayT;
        }
        public void setCompleteBillspayT(Integer completeBillspayT) {
            COMPLETE_BILLSPAY_T = completeBillspayT;
        }
        public void setInitiateTopupT(Integer initiateTopupT) { INITIATE_TOPUP_T = initiateTopupT;}
        public void setCompleteTopupT(Integer completeTopupT) { COMPLETE_TOPUP_T = completeTopupT;}
        public void setTopupRewardBilling(Integer topupRewardBilling) {
            TOPUP_REWARD_BILLING = topupRewardBilling;
        }
        public void setTopupRewardReceiver(Integer topupRewardReceiver) {
            TOPUP_REWARD_RECEIVER = topupRewardReceiver;
        }
        public void setTopupRewardSender(Integer topupRewardSender) {
            TOPUP_REWARD_SENDER = topupRewardSender;
        }
        public void setInitiateTopupIflix(Integer initiateTopupIflix) {
            COMPLETE_TOPUP_IFLIX = initiateTopupIflix;
        }
        public void setCompleteTopupIflix(Integer completeTopupIflix) {
            COMPLETE_TOPUP_IFLIX = completeTopupIflix;
        }
        public void setInitiateTopup(Integer InitiateTopup) {
            INITIATE_TOPUP = InitiateTopup;
        }
        public void setCompleteTopup(Integer completeTopup) {
            COMPLETE_TOPUP = completeTopup;
        }
    }

    public void setADDRS(Addrs ADDRS) {
        TopupConstants.ADDRS = ADDRS;
    }

    public void setURIS(Uris URIS) {
        TopupConstants.URIS = URIS;
    }

    public void setWALLETS(Wallets WALLETS) {
        TopupConstants.WALLETS = WALLETS;
    }

    public void setTransTypeId(TransTypeId transTypeId) {
        TRANS_TYPE_ID = transTypeId;
    }

    public void setServiceCode(ServiceCode serviceCode) {SERVICE_CODE = serviceCode;}

    public void setServiceProvider(ServiceProvider serviceProvider) {
        SERVICE_PROVIDER = serviceProvider;
    }

    public void setTransactionType(TransactionType transactionType) {
        TRANSACTION_TYPE = transactionType;
    }
}
