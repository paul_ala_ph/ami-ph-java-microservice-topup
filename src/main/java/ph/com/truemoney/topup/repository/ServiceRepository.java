package ph.com.truemoney.topup.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ph.com.truemoney.topup.dto.*;
import ph.com.truemoney.topup.entities.ServiceEntry;

import java.util.List;

public interface ServiceRepository extends JpaRepository<ServiceEntry, Long> {
	@Query("select new ph.com.truemoney.topup.dto.PostTransactionDetailsDTO(" +
			"s.id, " +
			"s.hasInsuranceAdvertisement, " +
			"s.hasReversalMessage, " +
			"s.serviceCategory.id, " +
			"s.productCode, " +
            "sp.id, " +
            "sp.serviceProvider.id, " +
            "sp.serviceProductCode, " +
            "s.category, sp.keys1, sp.keys2, sp.keys3, sp.keys4, "+
            "sp.withAmountField, sp.firstFieldLabel, sp.secondFieldLabel, sp.acceptsPastDue) " +
            "from ServiceEntry as s " +
            "join s.serviceProductList as sp " +
            "join sp.serviceProvider as spr " +
            "where s.productCode = :productCode order by sp.priority asc")
	List<PostTransactionDetailsDTO> findDetailsByProductCode(@Param("productCode") String productCode);


	ServiceEntry findByProductCode(String productCode);
}
