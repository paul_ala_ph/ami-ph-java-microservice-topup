package ph.com.truemoney.topup.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming( PropertyNamingStrategy.UpperCamelCaseStrategy.class )
public class InitiateTopupResponse {
	private String transactionId;
    private String amount;
    private String fee;
    private String commission;
    private String discount;
    private String totalAmount;
    private String hasReversalMessage;

    public InitiateTopupResponse() {
    	transactionId = "0";
    	amount = "0.00";
    	fee = "0.00";
    	commission = "0.00";
    	discount = "0.00";
    	totalAmount = "0.00";
    	hasReversalMessage = "0";
    }
}
