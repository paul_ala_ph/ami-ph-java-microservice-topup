package ph.com.truemoney.topup.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming( PropertyNamingStrategy.UpperCamelCaseStrategy.class )
public class CompleteTopupResponse {
	private String transactionId;
    private String transactionDate;
    private String amount;
    private String fee;
    private String commission;
    private String discount;
    private String oldBalance;
    private String newBalance;

    public CompleteTopupResponse() {
    	transactionId = "0";
    	amount = "0.00";
    	fee = "0.00";
    	commission = "0.00";
    	discount = "0.00";
        oldBalance = "0.00";
        newBalance = "0.00";
    }
}
