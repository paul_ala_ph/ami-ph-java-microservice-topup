package ph.com.truemoney.topup.services.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ph.com.truemoney.topup.dto.*;
import ph.com.truemoney.topup.entities.ServiceCategory;
import ph.com.truemoney.topup.entities.ServiceEntry;
import ph.com.truemoney.topup.repository.ServiceRepository;
import ph.com.truemoney.topup.services.RepoService;
import ph.com.truemoney.utils.MapperUtil;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
public class RepoServiceImpl implements RepoService{

	@Autowired
	private ServiceRepository servicesRepository;
	
	@Override
	public PostTransactionDetailsDTO getProductDetailsByProductCode(String productCode) {
		List<PostTransactionDetailsDTO> serviceProduct = servicesRepository.findDetailsByProductCode(productCode);
		if (serviceProduct.isEmpty())
			return null;
		else
			return serviceProduct.get(0);
	}

}
