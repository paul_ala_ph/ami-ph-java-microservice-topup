package ph.com.truemoney.topup.services;

import ph.com.truemoney.base.response.EndpointResult;
import ph.com.truemoney.topup.request.CompleteTopupRequest;
import ph.com.truemoney.topup.request.InitiateTopupRequest;
import ph.com.truemoney.topup.request.Topup;
import ph.com.truemoney.topup.request.TopupN3;

/**
 * Topup Service Interface.
 */
public interface TopupService {

    //helper services
    String convertMobileNumber(String mobileNumber);

    EndpointResult initiateTopup(Topup request);
    EndpointResult initiateTopupN3(TopupN3 request);
    EndpointResult initiateTopupPhone(Topup request);
    EndpointResult initiateTopupExternal(InitiateTopupRequest request);
    

    EndpointResult completeTopUp(Topup request);
    EndpointResult completeTopUpN3(TopupN3 request);
    EndpointResult completeTopupPhone(Topup request);
    EndpointResult completeTopupExternal(CompleteTopupRequest request);
}
