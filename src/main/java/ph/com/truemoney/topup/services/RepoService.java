package ph.com.truemoney.topup.services;


import ph.com.truemoney.topup.dto.PostTransactionDetailsDTO;


public interface RepoService {
	PostTransactionDetailsDTO getProductDetailsByProductCode(String productCode);
}
