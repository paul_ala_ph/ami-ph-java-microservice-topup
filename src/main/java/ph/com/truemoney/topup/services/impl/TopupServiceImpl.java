package ph.com.truemoney.topup.services.impl;

import lombok.extern.log4j.Log4j2;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import ph.com.truemoney.base.client.ApiClient;
import ph.com.truemoney.base.response.EndpointResult;
import ph.com.truemoney.topup.dto.PostTransactionDetailsDTO;
import ph.com.truemoney.topup.dto.ServiceTransactionDTO;
import ph.com.truemoney.topup.entities.ServiceEntry;
import ph.com.truemoney.topup.model.LoadCentral;
import ph.com.truemoney.topup.model.ThirdPartyResult;
import ph.com.truemoney.topup.model.Transactions;
import ph.com.truemoney.topup.repository.ServiceRepository;
import ph.com.truemoney.topup.request.*;
import ph.com.truemoney.topup.constants.TopupConstants;
import ph.com.truemoney.topup.constants.TopupConstants.*;
import ph.com.truemoney.topup.response.CompleteTopupResponse;
import ph.com.truemoney.topup.response.InitiateTopupResponse;
import ph.com.truemoney.topup.services.RepoService;
import ph.com.truemoney.topup.services.TopupService;
import ph.com.truemoney.topup.utility.MobileNumberUtil;
import ph.com.truemoney.utils.MapperUtil;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static ph.com.truemoney.utils.MapperUtil.jsonToObject;
import static ph.com.truemoney.utils.MapperUtil.objectToJson;

/**
 * The type Topup service.
 */
@Log4j2
@Service
public class TopupServiceImpl implements TopupService {

    @Autowired
    private ServiceRepository servicesRepository;

    @Autowired
    private RepoService repoService;

    /**
     * CONSTANT
     **/
    private static Long CON_ZERO = 0L;

    @Override
    public EndpointResult initiateTopup(Topup request) {
        log.debug( "----------DEBUG START initiateTopup Service--------------" );
        log.debug( "----------[initiateTopup request]" + objectToJson( request ) + "--------------" );
        EndpointResult response = new EndpointResult( );


        ServiceEntry infoSvc = servicesRepository.findByProductCode( request.getProductId( ) );
        if ( infoSvc == null ) {
            log.debug( "----------[initiateTopup] old route start--------------" );
            //"http://10.14.20.171:82/Core/EDC/EDC.svc/RESTful/initiateTopup";
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTINITIATETOPUPEDC;
            HttpEntity< MultiValueMap< String, String > > spiRequest = new HttpEntity( request );
            String respObject = ApiClient.postForObject( uri, spiRequest, String.class );
            log.debug( "----------[initiateTopup core response]" + respObject + "--------------" );
            EndpointResult result = convertCoreResponse( respObject );
            log.debug( "----------[initiateTopup] old route end--------------" );
            return result;
        }

        try {
            request.setCustomerMobileNumber(convertMobileNumber(request.getCustomerMobileNumber()));

            //#region Check if mobile number if in blocklist
            Boolean isMobileValid = this.validateMobileNumber( request.getTransactionType(), request );
            if ( !isMobileValid ) {
                log.debug( "----------mobile number is invalid.--------------" );
                response.setResponseCodeMessage( "-101" );
                return response;
            }
            // #endregion

            // #region Get sourceWalletId
            Long sourceWalletId = this.getSourceWalletId( request );
//            if ( CON_ZERO.equals( sourceWalletId ) ) {
//                log.debug( "----------source wallet id not found--------------" );
//                response.setResponseCodeMessage( "27" );
//                return response;
//            }
            // #endregion

            // #region Product Validation
            PostTransactionDetailsDTO validationResult = null;
            Map< String, String > validateProductResult = validateProductCode(request.getProductId(), request.getTransactionAmount());
            Map< String, String > dataDR = new HashMap< String, String >( );

            if(validateProductResult != null){
                if ( validateProductResult.containsKey( "validationResult" ) ) {
                    validationResult = jsonToObject( validateProductResult.get( "validationResult" ),
                            PostTransactionDetailsDTO.class );

                    //#region Get transactionTypeId and targetWalletID based in serviceProviderId
                    Integer transactionTypeId = request.getTransactionType();
                    Long targetWalletId = Wallets.PALOAD;
                    if(validationResult.getServiceProviderId().equals(ServiceProvider.TOPPSDIGITALSERVICES)){
                        transactionTypeId = TransactionType.INITIATE_TOPUP_IFLIX;
                        targetWalletId = Wallets.IFLIX;
                    }
                    //#endregion

                    if(validationResult.getResultCode().equals("0")){
                        //#region Get Amount based in serviceProviderId
                        validationResult.setKeys2(validationResult.getKeys2().isEmpty() ? "0" :
                                validationResult.getKeys2());

                        if (validationResult.getServiceProviderId() == Long.parseLong(ServiceProvider.ECPAY.toString())
                                && !validationResult.getKeys1().isEmpty()){
                            request.setTransactionAmount(validationResult.getKeys2());
                        } else if (validationResult.getServiceProviderId() == Long.parseLong(ServiceProvider.BAYADCENTER.toString())) {
//PAULTESTING>>watchout
                            request.setTransactionAmount(validationResult.getKeys2() + validationResult.getKeys3());
                        }

                        //#region Wallet Movement
                        // Get Agent Card ID
                        Long agentWalletId = 0L;
                        log.debug( "----------[initiateBillingV2] get agentWalletId--------------" );
                        log.debug( "----------cardNumber:"+ request.getAgentCardNumber( ) +"--------------" );
                        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getWalletIdbyCardNumber?cardNumber={cardNumber}";
                        String uriAgentWallet = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETWALLETID;
                        Map< String, Object > params = new HashMap<>( );
                        params.clear( );
                        params.put( "cardNumber", request.getAgentCardNumber( ) );
                        String respStringAgentId = ApiClient.getForObject( uriAgentWallet, String.class, params );
                        log.debug( "----------[initiateBillingV2 agentWalletId response]" + respStringAgentId + " --------------" );
                        EndpointResult< String > agentIdResult = convertCoreResponse( respStringAgentId );
                        agentWalletId = Long.valueOf( agentIdResult.getData( ) );

                        // =======================================================

                        EndpointResult<Transactions> processTransResp = this.processTransaction(
                                agentWalletId, sourceWalletId, targetWalletId,
                                request.getTransactionAmount( ), request.getTerminalId( ), transactionTypeId,
                                request.getChannelTypeId( ).toString( ), StringUtils.EMPTY,
                                request.getProductId( ), request.getAgentCardNumber( ), StringUtils.EMPTY, StringUtils.EMPTY,
                                StringUtils.EMPTY, StringUtils.EMPTY,
                                StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, 0.00, 0, request.getPartnerRefNo( ),
                                request.getAdditionalInformation4( ), StringUtils.EMPTY, StringUtils.EMPTY);

                        if(processTransResp==null){
                            log.info("processTransResp is null...");
                            response.setResponseCode("22");
                            return response;
                        }

                        log.info( "processTransResp response code: " + processTransResp.getResponseCode( ) );

                        //added to validate response from processTrans
                        if(!processTransResp.getResponseCode().equalsIgnoreCase("0")){
                            log.debug( "----------[initiateBillingV2] processTransResp failed.--------------" );
                            response.setResponseCodeMessage(processTransResp.getResponseCode());
                            return response;
                        }
                        Transactions initiateTopUp = new Transactions( );
                        String strTransaction = objectToJson( processTransResp.getData( ) );
                        initiateTopUp = jsonToObject( strTransaction, Transactions.class );

                        dataDR.put("TransactionId",initiateTopUp.getTransactionId());
                        request.setServiceProductId(validationResult.getServiceProductId().toString());

                        EndpointResult saveResult = insertServiceTransaction(validationResult.getServiceCategoryId(), request,
                                initiateTopUp.getTransactionId(), 1, validationResult.getServiceProviderId());
//paultesting>>watch out for this
                        if(saveResult.getData()==null || !saveResult.getResponseCode().equals("0")){
                            response.setResponseCodeMessage(saveResult.getResponseCode());
                            return response;
                        }

                        if(initiateTopUp.getResponseCode().equals("0")){
                            dataDR.put("ProductId",request.getProductId());
                            dataDR.put("TransactionAmount", initiateTopUp.getTransactionAmount().isEmpty() ? "0" : initiateTopUp.getTransactionAmount());
                            dataDR.put("CashoutFee", initiateTopUp.getCashoutFee());
                        }
                        response.setResponseCode(initiateTopUp.getResponseCode());

                        updateServiceTransaction( initiateTopUp.getTransactionId( ), dataDR, request,
                                request.getCustomerMobileNumber(), StringUtils.EMPTY, 3, null);
                    }else{
                        log.debug( "----------validationResult is error--------------" );
                        response.setResponseCodeMessage(validationResult.getResultCode());
                    }
                    //#endregion
                }else{
                    log.debug( "----------validationResult is empty--------------" );
                    response.setResponseCodeMessage("106");
                }
                //#endregion
            }else{
                log.debug( "----------validateProductResult is null--------------" );
                response.setResponseCodeMessage("106");
            }

            response.setData(dataDR);
            //#endregion
        } catch(Exception e){
            log.error( "Failed to connect to core" );
            e.printStackTrace( );
            response.setResponseCode("22");
        }
        return response;
    }

    @Override
    public EndpointResult initiateTopupPhone(Topup request) {
        EndpointResult response = new EndpointResult();

        if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                !request.getCustomerMobileNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                !request.getTransactionAmount().isEmpty() && request.getTransactionType().equals(TransactionType.INITIATE_TOPUP)){

            //Non True
            if((request.getProductId().contains("PLP") && !request.getCustomerTelephoneNo().isEmpty()) ||
                    !request.getProductId().contains("PLP") ){
                response = this.initiateTopup( request );
                return response;
            }else if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                    !request.getCustomerMobileNumber().isEmpty() && !request.getSourceCardNumber().isEmpty() &&
                    !request.getTerminalId().isEmpty() && !request.getTransactionAmount().isEmpty() &&
                    request.getTransactionType().equals(TransactionType.INITIATE_TOPUP_T)){
                //True
                response = this.initiateTopup( request );
                return response;
            }
            response.setResponseCode("1");
            return response;
        }
        return response;
    }

    @Override
    public EndpointResult initiateTopupN3(TopupN3 request) {
        EndpointResult response = new EndpointResult();

        if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                !request.getCustomerMobileNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                !request.getTransactionAmount().isEmpty() && request.getTransactionType().equals(TransactionType.INITIATE_TOPUP)){

            Topup topupRequest = this.n3ToEdcTopup(request);

            //Non True
            if((request.getProductId().contains("PLP") && !request.getCustomerMobileNumber().isEmpty()) ||
                    !request.getProductId().contains("PLP") ){
                response = this.initiateTopup( topupRequest );
                return response;
            }else if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                    !request.getCustomerMobileNumber().isEmpty() && !request.getSourceCardNumber().isEmpty() &&
                    !request.getTerminalId().isEmpty() && !request.getTransactionAmount().isEmpty() &&
                    request.getTransactionType().equals(TransactionType.INITIATE_TOPUP_T)){
                //True
                response = this.initiateTopup( topupRequest );
                return response;
            }
            response.setResponseCode("1");
            return response;
        }

        return response;
    }

    @Override
    public EndpointResult initiateTopupExternal(InitiateTopupRequest parameter) {
        EndpointResult response = new EndpointResult();


        ServiceEntry infoSvc = servicesRepository.findByProductCode( parameter.getProductCode( ) );
        if ( infoSvc == null ) {
            log.debug( "----------[initiateTopupExternal] old route start--------------" );
            //"http://10.14.20.171:82/Core/External/External.svc/External/initiateTopup";
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTINITIATETOPUPEXTERNAL;
            HttpEntity< MultiValueMap< String, String > > spiRequest = new HttpEntity( parameter );
            String respObject = ApiClient.postForObject( uri, spiRequest, String.class );
            log.debug( respObject );
            log.debug( "----------[initiateTopupExternal resp]" + objectToJson( respObject ) + " --------------" );
            EndpointResult result = convertCoreResponse( respObject );
            log.debug( "----------[initiateTopupExternal] old route end--------------" );
            return result;
        }

        //#region Amount Validation
        if(!(parameter.getAmount() > 0)){
            log.debug( "----------[initiateTopupExternal] amount is zero...--------------" );
            response.setResponseCodeMessage("34");
            return response;
        }
        //end region

        Topup request = new Topup();
        request.setTransactionType(TransactionType.INITIATE_TOPUP);
        request.setProductId(parameter.getProductCode());
        request.setCustomerMobileNumber(parameter.getCustomerMobileNumber());
        request.setTransactionAmount(parameter.getAmount().toString());
        request.setPartnerRefNo(parameter.getPartnerRefNo());

        if(!parameter.getLandline().isEmpty() && !parameter.getLandline().equals("0")){
            request.setCustomerTelephoneNo(parameter.getLandline());
        }else if(!parameter.getServiceId().isEmpty() && !parameter.getServiceId().equals("0")){
            request.setCustomerTelephoneNo(parameter.getServiceId());
        }else if (!parameter.getAccountNo().isEmpty() && !parameter.getAccountNo().equals("0")){
            request.setCustomerTelephoneNo(parameter.getAccountNo());
        }


        //http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getItemNumberByKitId?kitId={kitId}&itemId={itemId}
        String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETITEMNUMBERBYKITID;
        log.debug( "----------[initiateTopupExternal] GETITEMNUMBERBYKITID...--------------" );
        log.debug( "----------kitId: " + parameter.getTmnCenterId() + " || itemId: 2--------------" );
        Map< String, Object > params = new HashMap< String, Object >( );
        params.put( "kitId", parameter.getTmnCenterId() );
        params.put( "itemId", "2" );
        String respString = ApiClient.getForObject( uri, String.class, params );
        EndpointResult< String > terminalIdResult = convertCoreResponse( respString );
        if ( !terminalIdResult.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[initiateTopupExternal] GETITEMNUMBERBYKITID not found...--------------" );
            response.setResponseCodeMessage( "22" );
            return response;
        }
        request.setTerminalId( terminalIdResult.getData( ) );

        EndpointResult< String > walletIdResponse = getWalletIDFromCore( parameter.getTrueMoneyId( ),
                parameter.getTmnCenterId() );
        if ( !walletIdResponse.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[initiateTopupExternal] GETWALLETIDBYKITID not found...--------------" );
            response.setResponseCodeMessage( "22" );
            return response;
        }
        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getCardNumber?walletId={walletId}";
        String uriAC = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETCARDNUMBER;
        log.debug( "----------[initiateTopupExternal] GETCARDNUMBER...--------------" );
        log.debug( "----------walletId: " + walletIdResponse.getData( ).toString( ) + " --------------" );
        params.clear( );
        params.put( "walletId", walletIdResponse.getData( ).toString( ) );
        String respStringWalletId = ApiClient.getForObject( uriAC, String.class, params );
        EndpointResult< String > agentCardResult = convertCoreResponse( respStringWalletId );
        if ( !agentCardResult.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[initiateTopupExternal] GETCARDNUMBER not found...--------------" );
            response.setResponseCodeMessage( "22" );
            return response;
        }
        request.setAgentCardNumber( agentCardResult.getData( ) );
        request.setSourceCardNumber( "" );
        request.setChannelTypeId( parameter.getChannelTypeId( ) );

        // CALL initiateTopup
        EndpointResult< Map< String, String > > initResponse = initiateTopup( request );
        InitiateTopupResponse initResp = new InitiateTopupResponse( );
        Map< String, String > respData = new HashMap< String, String >( );

        if ( initResponse.getData( ) instanceof Map ) {
            respData = initResponse.getData( );
        }

        Long transId = 0L;
        log.debug( "----------[initiateTopupExternal] after initiateTopup--------------" );
        log.debug( "----------[initiateTopupExternal data]" + objectToJson( initResponse ) + "--------------" );
        if ( initResponse.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[initiateTopupExternal] initiateTopup valid--------------" );
            initResp.setTransactionId( respData.get( "TransactionId" ) );
            initResp.setAmount( checkTransAmountLength( respData.get( "TransactionAmount" ) ) );
            initResp.setFee( respData.get( "CashoutFee" ) );
            initResp.setDiscount( respData.get( "Discount" ) );

            log.info( "respData reversalMessage: " + respData.get( "HasReversalMessage" ) );
            response.addData( initResp );
            response.setResponseCode("0");
        } else {
            log.debug( "----------[initiateBillsPaymentV2] initiateBillingV2 error--------------" );
            response.setData( null );
            response.setResponseCodeMessage( initResponse.getResponseCode( ) );
            response.setResponseMessage( initResponse.getResponseMessage( ) );
        }

        log.debug( "----------[initiateTopupExternal] response>>"+response+"--------------" );
        return response;
    }

    @Override
    public EndpointResult completeTopUp(Topup request) {
        log.debug( "----------DEBUG START completeTopUp Service--------------" );
        log.debug( "----------[completeTopUp request]" + objectToJson( request ) + "--------------" );
        EndpointResult response = new EndpointResult( );

        ServiceEntry infoSvc = servicesRepository.findByProductCode( request.getProductId( ) );
        if ( infoSvc == null ) {
            log.debug( "----------[completeTopUp] old route start--------------" );
            //"http://10.14.20.171:82/Core/EDC/EDC.svc/RESTful/CompleteTopUp";
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTCOMPLETETOPUPEDC;
            HttpEntity< MultiValueMap< String, String > > spiRequest = new HttpEntity( request );
            String respObject = ApiClient.postForObject( uri, spiRequest, String.class );
            log.debug( "----------[completeTopUp core response]" + respObject + "--------------" );
            EndpointResult result = convertCoreResponse( respObject );
            log.debug( "----------[completeTopUp] old route end--------------" );
            return result;
        }

        try {
            request.setCustomerMobileNumber(convertMobileNumber(request.getCustomerMobileNumber()));

            // #region Get sourceWalletId
            Long sourceWalletId = this.getSourceWalletId( request );
            if ( CON_ZERO.equals( sourceWalletId ) ) {
                log.debug( "----------source wallet id not found--------------" );
                response.setResponseCodeMessage( "27" );
                return response;
            }
            // #endregion

            // #region Validate if duplicate Transaction
            Integer resultChecking = validateIfDuplicateTransaction( request.getTransactionType( ), request.getTransactionID().toString() );
            if ( resultChecking != 0 ) {
                log.debug( "----------[completeTopup] duplicated transaction.--------------" );
                response.setResponseCode( resultChecking.toString( ) );
                return response;
            }

            // #region Product Validation
            String providerName = StringUtils.EMPTY;
            String rrn = StringUtils.EMPTY;
            String originalThirdPartyResponse = StringUtils.EMPTY;
            PostTransactionDetailsDTO validationResult = null;
            Map< String, String > validateProductResult = validateProductCode(request.getProductId(), StringUtils.EMPTY);
            Map< String, String > dataDR = new HashMap<>( );
            Map< String, String > requestMap = new HashMap<>( );
            ThirdPartyResult thirdPartyResult = new ThirdPartyResult( );

            if(validateProductResult != null){
                if ( validateProductResult.containsKey( "validationResult" ) ) {
                    validationResult = jsonToObject( validateProductResult.get( "validationResult" ),
                            PostTransactionDetailsDTO.class );

                    if(validationResult == null){
                        log.debug( "----------validationResult is empty--------------" );
                        response.setResponseCodeMessage("106");
                        return response;
                    }

                    //#region Get targetWalletID based in provider
                    Long targetWalletId = Wallets.PALOAD;
                    if(validationResult.getServiceProviderId().equals(ServiceProvider.TOPPSDIGITALSERVICES)){
                        targetWalletId = Wallets.IFLIX;
                    }
                    //#endregion

                    if(validationResult.getResultCode().equals("0")){
                        //#region Get Amount based in serviceProviderId
                        validationResult.setKeys2(validationResult.getKeys2().isEmpty() ? "0" :
                                validationResult.getKeys2());

                        if (validationResult.getServiceProviderId() == Long.parseLong(ServiceProvider.ECPAY.toString())
                                && !validationResult.getKeys1().isEmpty()){
                            request.setTransactionAmount(validationResult.getKeys2());
                        } else if (validationResult.getServiceProviderId() == Long.parseLong(ServiceProvider.BAYADCENTER.toString())) {
//paultesting>>watchout
                            request.setTransactionAmount(validationResult.getKeys2() + validationResult.getKeys3());
                        }
                        //#endregion

                        requestMap.put("ProductDescription",validationResult.getProductDescription());
                        requestMap.put("ProductId",request.getProductId());
                        requestMap.put("TransactionAmount",request.getTransactionAmount());
                        requestMap.put("CustomerMobileNumber",request.getCustomerMobileNumber());
                        if (!request.getCustomerTelephoneNo().isEmpty())
                            requestMap.put("CustomerTelephoneNumber",request.getCustomerTelephoneNo());

                        //#region Check for Topup Reward
                        if(request.getTransactionType() == TransactionType.TOPUP_REWARD_BILLING ||
                                request.getTransactionType() == TransactionType.TOPUP_REWARD_SENDER ||
                                request.getTransactionType() == TransactionType.TOPUP_REWARD_RECEIVER){
                            EndpointResult saveResult = insertServiceTransaction( validationResult.getServiceCategoryId( ), request,
                                    request.getTransactionID().toString(), 1, validationResult.getServiceProviderId( )
                            );
//paultesting>>watch out for this
                            if(saveResult.getData()==null || !saveResult.getData().equals(0)){
                                log.debug( "----------[completeTopup] INSERT SERVICE TRANSACTION FAILED.--------------" );
                                response.setResponseCodeMessage(saveResult.getResponseCode());
                                return response;
                            }
                        }
                        //#endregion


                        log.debug( "----------[completeTopup] SERVICE PROVIDER IS>>"+validationResult.getServiceProviderId()+"--------------" );
                        if(validationResult.getServiceProviderId().equals(ServiceProvider.LOADCENTRAL.longValue())){
                            //#region LOADCENTRAL
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "SSS" );
                            LocalDateTime dateNow = LocalDateTime.now( );
                            String dateTime = dateNow.format( formatter );
                            RandomString randomAlphaNumericString = new RandomString(6);

                            providerName = "Load Central";
                            rrn = "TMN" + randomAlphaNumericString.toString() + dateTime;

                            LoadCentral apiResult = new LoadCentral();
                            if((request.getProductId().contains("PLP") || validationResult.getKeys4().equalsIgnoreCase("Y"))){
                                updateServiceTransaction( request.getTransactionID().toString(), "", request,
                                        request.getCustomerTelephoneNo(), rrn, 1, null);
//paultesting>> to do tomorrow SPI
                                //apiResult = LoadCentralServices.LoadCentral_SellProduct(validationResult.serviceProductCode, topup.customerTelephoneNo, RRN);
                            }else{
                                updateServiceTransaction( request.getTransactionID().toString(), "", request,
                                        request.getCustomerMobileNumber(), rrn, 1, null);
//paultesting>> to do tomorrow SPI
                                //apiResult = LoadCentralServices.LoadCentral_SellProduct(validationResult.serviceProductCode, topup.customerTelephoneNo, RRN);
                            }

                            if(!apiResult.getRESP().equalsIgnoreCase("0"))
                                apiResult.setORESP(apiResult.getRESP());
//paultesting>> to do tomorrow SPI
                            //apiResult = LoadCentralServices.ConvertLoadCentralErrorCode(apiResult, topup);

                            originalThirdPartyResponse = MapperUtil.objectToJson(apiResult);

                            thirdPartyResult.setRESP( apiResult.getRESP() );
                            thirdPartyResult.setERR( apiResult.getERR() );
                            thirdPartyResult.setRRN( apiResult.getBAL() );
                            thirdPartyResult.setOERR( apiResult.getOERR() );
                            thirdPartyResult.setORESP( apiResult.getORESP() );
                            thirdPartyResult.setEPIN( apiResult.getEPIN() );
                            thirdPartyResult.setBAL( apiResult.getBAL() );
                            thirdPartyResult.setTID( apiResult.getTID() );

//                            if(thirdPartyResult.getRESP().equalsIgnoreCase("0")){
//                                //#region REMAINING BALANCE OF LOADCENTRAL SMS NOTIFICATION
//                                //TODO:sendRemainingBalanceSmsNotification
//                                //#endregion
//                            }
                        }else{
                            log.debug( "----------Service Provider not available--------------" );
                            response.setResponseCodeMessage("31");
                            return response;
                        }

                        if(thirdPartyResult != null){
                            if(thirdPartyResult.getRESP().equalsIgnoreCase("0")){
                                //#region Success Response
                                //#region Get initiatorWalletId based in transactionType
                                Long iniatorWalletId = 0L;
                                if(request.getTransactionType() == TransactionType.TOPUP_REWARD_BILLING ||
                                        request.getTransactionType() == TransactionType.TOPUP_REWARD_SENDER ||
                                        request.getTransactionType() == TransactionType.TOPUP_REWARD_RECEIVER){
                                    iniatorWalletId = sourceWalletId;
                                }else{
                                    log.debug( "----------[completeTopup] get agentWalletId--------------" );
                                    //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getWalletIdbyCardNumber?cardNumber={cardNumber}";
                                    String uriAgentWallet = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETWALLETID;
                                    Map< String, Object > params = new HashMap<>( );
                                    params.clear( );
                                    params.put( "cardNumber", request.getAgentCardNumber( ) );
                                    String respStringAgentId = ApiClient.getForObject( uriAgentWallet, String.class, params );
                                    log.debug( "----------[completeTopup agentWalletId response]" + respStringAgentId + " --------------" );
                                    EndpointResult< String > agentIdResult = convertCoreResponse( respStringAgentId );
                                    iniatorWalletId = Long.valueOf( agentIdResult.getData( ) );
                                }
                                //#endregion

                                //#region Get transactionTypeId based in serviceProviderId
                                Integer transactionTypeId = request.getTransactionType();
                                if(validationResult.getServiceProviderId().equals(ServiceProvider.TOPPSDIGITALSERVICES)){
                                    transactionTypeId = TransactionType.COMPLETE_TOPUP_IFLIX;
                                }
                                //#endregion


                                // =======================================================

                                EndpointResult<Transactions> processTransResp = this.processTransaction(
                                        iniatorWalletId, sourceWalletId, targetWalletId,
                                        request.getTransactionAmount( ), request.getTerminalId( ), transactionTypeId,
                                        request.getChannelTypeId( ).toString( ), request.getTransactionID().toString(),
                                        request.getProductId( ), request.getAgentCardNumber( ), StringUtils.EMPTY, StringUtils.EMPTY,
                                        StringUtils.EMPTY, StringUtils.EMPTY,
                                        StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, 0.00, 0, request.getPartnerRefNo( ),
                                        request.getAdditionalInformation4( ), StringUtils.EMPTY, StringUtils.EMPTY);

                                if(processTransResp==null){
                                    log.info("processTransResp is null...");
                                    response.setResponseCode("22");
                                    return response;
                                }

                                log.info( "processTransResp response code: " + processTransResp.getResponseCode( ) );

                                Transactions completeTopUp = new Transactions( );
                                String strTransaction = objectToJson( processTransResp.getData( ) );
                                completeTopUp = jsonToObject( strTransaction, Transactions.class );

                                //added to validate response from processTrans
                                if(!processTransResp.getResponseCode().equalsIgnoreCase("0")){
                                    //#region Send Email Success In ThirdParty Failed In Core
//        							if(processTransResp.getResponseCode().equalsIgnoreCase("94")){
//        									processTransResp.setResponseCodeMessage("22");
//        								//TODO: sendEmailSuccessInThirdPartyFailedInCore
//        							}
//        							//#endregion


                                    dataDR.put("LoadCentral_Message",StringUtils.EMPTY);
                                    dataDR.put("TransactionId",completeTopUp.getTransactionId());
                                    response.setResponseCodeMessage(completeTopUp.getResponseCode());
                                    return response;
                                }else{
                                    dataDR.put( "CommissionAmount", completeTopUp.getCommissionAmount( ) );
                                    dataDR.put( "CashoutFee", completeTopUp.getCashoutFee( ) );
                                    dataDR.put( "OldBalance", completeTopUp.getSourceOldBalance( ) );
                                    dataDR.put( "NewBalance", completeTopUp.getSourceNewBalance( ) );
                                    dataDR.put("LoadCentral_ReferenceNumber",thirdPartyResult.getRRN());
                                    dataDR.put("LoadCentral_Status",thirdPartyResult.getORESP());
                                    dataDR.put("LoadCentral_Message",thirdPartyResult.getOERR());

                                    dataDR.put( "TransactionId", completeTopUp.getTransactionId( ) );
                                    dataDR.put( "Epin", thirdPartyResult.getEPIN( ) );
                                    dataDR.put( "ResponseCode", "0");
                                    dataDR.put( "TransactionResponseCode", thirdPartyResult.getRESP( ) );
                                    dataDR.put( "TransactionResponseMessage", thirdPartyResult.getERR( ) );

                                    //#region Send SMS
                                    log.debug( "----------[completeTopup sending sms]--------------" );
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "MM-dd-YYYY hh:mm:ss a" );
                                    LocalDateTime dateNow = LocalDateTime.now( );
                                    String dateTime = dateNow.format( formatter );

                                    SmsRequest smsReq = new SmsRequest( );
                                    smsReq.setTo( request.getCustomerMobileNumber( ) );
                                    Map< String, String > smsParam = new HashMap< String, String >( );
                                    smsParam.put( "initiateTransId", request.getTransactionID( ).toString() );
                                    smsParam.put( "completeTransId", completeTopUp.getTransactionId() );
                                    smsParam.put( "transTypeId", request.getTransactionType().toString() );
                                    smsParam.put( "productCode", request.getProductId() );
                                    smsParam.put( "receiverMobileNumber", request.getCustomerMobileNumber() );
                                    smsParam.put( "transAmount", request.getTransactionAmount( ) );
                                    smsParam.put( "dateOfTransaction", dateTime );
                                    smsParam.put( "rrn", rrn );
                                    smsParam.put( "pin", thirdPartyResult.getEPIN());
                                    smsParam.put( "serviceProviderId", validationResult.getServiceProviderId().toString());

                                    smsReq.setParams( smsParam );
                                    String notifUrl = TopupConstants.ADDRS.NOTIFADDR + TopupConstants.URIS.POSTNOTIFSEND;
                                    try {
                                        String smsResp = ApiClient.postForObject( notifUrl, smsReq, String.class );
                                        log.debug( "----------[completeTopup sms resp]" + smsResp + "--------------" );
                                    } catch ( Exception e ) {
                                        log.debug( "----------[completeTopup sms resp exception]--------------" );
                                        log.debug( e.getMessage( ) );
                                    }
                                    //#endregion
                                }
                                //#endregion
                            }else{
                                //#region Failed Response
                                log.debug( "Complete Top Up. Status Failed. Error Code:" +thirdPartyResult.getOERR() );
                                dataDR.put("LoadCentral_Status",thirdPartyResult.getORESP());
                                dataDR.put("LoadCentral_Message",thirdPartyResult.getOERR());
                                dataDR.put( "TransactionId", request.getTransactionID( ).toString() );
                                response.setResponseCodeMessage(thirdPartyResult.getRESP());
                                //#endregion
                            }

                            updateServiceTransaction( request.getTransactionID( ).toString(), originalThirdPartyResponse, null,
                                    "", rrn, 2, thirdPartyResult);

                        }else{
                            log.debug( "----------thirdPartyResult is NULL--------------" );
                            dataDR.put("LoadCentral_Message",StringUtils.EMPTY);
                            dataDR.put("TransactionId",request.getTransactionID().toString());
                            response.setResponseCodeMessage("25");
                        }
                        //#endregion
                    }else{
                        log.debug( "----------validationResult is error--------------" );
                        response.setResponseCodeMessage(validationResult.getResultCode());
                    }
                    //#endregion
                }else{
                    log.debug( "----------validationResult is empty--------------" );
                    response.setResponseCodeMessage("106");
                }
                //#endregion
            }else{
                log.debug( "----------validateProductResult is null--------------" );
                response.setResponseCodeMessage("106");
            }

            response.setData(dataDR);
            return response;
        }catch (Exception e){
            log.error( "Failed to connect to core" );
            e.printStackTrace( );
            response.setResponseCode("22");
            return response;
        }
    }

    @Override
    public EndpointResult completeTopUpN3(TopupN3 request) {
        EndpointResult response = new EndpointResult();

        if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                !request.getCustomerMobileNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                !request.getTransactionAmount().isEmpty() && request.getTransactionType().equals(TransactionType.COMPLETE_TOPUP)){

            Topup topupRequest = this.n3ToEdcTopup(request);

            //Non True
            if((request.getProductId().contains("PLP") && !request.getCustomerMobileNumber().isEmpty()) ||
                    !request.getProductId().contains("PLP") ){
                response = this.completeTopUp( topupRequest );
                return response;
            }else if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                    !request.getSourceCardNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                    request.getTransactionType().equals(TransactionType.COMPLETE_TOPUP_T)){
                //True
                response = this.initiateTopup( topupRequest );
                return response;
            }
            response.setResponseCode("1");
            return response;
        }

        return response;

    }

    @Override
    public EndpointResult completeTopupPhone(Topup request) {
        EndpointResult response = new EndpointResult();

        if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                !request.getCustomerMobileNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                !request.getTransactionAmount().isEmpty() && request.getTransactionType().equals(TransactionType.COMPLETE_TOPUP)){

            //Non True
            if((request.getProductId().contains("PLP") && !request.getCustomerTelephoneNo().isEmpty()) ||
                    !request.getProductId().contains("PLP") ){
                response = this.completeTopUp(request);
                return response;
            }else if(!request.getAgentCardNumber().isEmpty() && !request.getProductId().isEmpty() &&
                    !request.getSourceCardNumber().isEmpty() && !request.getTerminalId().isEmpty() &&
                    !request.getTransactionAmount().isEmpty() && request.getTransactionType().equals(TransactionType.COMPLETE_TOPUP_T)){
                //True
                response = this.completeTopUp( request );
                return response;
            }
            response.setResponseCode("1");
            return response;
        }
        return response;
    }

    @Override
    public EndpointResult completeTopupExternal(CompleteTopupRequest parameter) {
        EndpointResult response = new EndpointResult();

        ServiceEntry infoSvc = servicesRepository.findByProductCode( parameter.getProductCode( ) );
        if ( infoSvc == null ) {
            log.debug( "----------[completeTopupExternal] old route start--------------" );
            //"http://10.14.20.171:82/Core/External/External.svc/External/initiateTopup";
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTCOMPLETETOPUPEXTERNAL;
            HttpEntity< MultiValueMap< String, String > > spiRequest = new HttpEntity( parameter );
            String respObject = ApiClient.postForObject( uri, spiRequest, String.class );
            log.debug( respObject );
            log.debug( "----------[completeTopupExternal resp]" + objectToJson( respObject ) + " --------------" );
            EndpointResult result = convertCoreResponse( respObject );
            log.debug( "----------[completeTopupExternal] old route end--------------" );
            return result;
        }

        Topup request = new Topup();

        if(parameter.getProductCode().isEmpty()){
            //#region Get topup details in transaction inquiry details
            //http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getTransactionInquiryDetails?transactionId={transactionId}&referenceNumber={referenceNumber}&transactionTypeId={transactionTypeId}
            log.debug( "----------[completeTopupExternal] GETTRANSACTIONINQUIRYDETAILS...--------------" );
            log.debug( "----------transactionId: " + parameter.getTransactionId( ) + " || referenceNumber:\"\" || transactionTypeId:0--------------" );
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETTRANSACTIONINQUIRYDETAILS;
            Map< String, Object > params = new HashMap< String, Object >( );
            params.put( "transactionId", parameter.getTransactionId( ) );
            params.put( "referenceNumber", "" );
            params.put( "transactionTypeId", "0" );

            String respObject = ApiClient.getForObject( uri, String.class, params );
            EndpointResult result = convertCoreResponse( respObject );

            if ( result.getData( ) == null ) {
                log.debug( "----------[completeTopupExternal] GETTRANSACTIONINQUIRYDETAILS failed.--------------" );
                response.setResponseCodeMessage( "16" );
                return response;
            }
            String strGetTransInq = objectToJson( result.getData( ) );
            Topup requestObj = new Topup();
            requestObj = MapperUtil.jsonToObject( strGetTransInq, Topup.class );

            request.setTransactionID(Integer.valueOf(parameter.getTransactionId()));
            request.setTransactionType(TransactionType.COMPLETE_TOPUP);
//paultesting>>watchout for this
            request.setProductId(requestObj.getProductId());
//paultesting>>watchout for this
            request.setCustomerMobileNumber(requestObj.getCustomerMobileNumber());
//paultesting>>watchout for this
            request.setTransactionAmount(requestObj.getTransactionAmount());
//paultesting>>watchout for this
            request.setPartnerRefNo(requestObj.getPartnerRefNo());
            //#endregion
        }else{
            //#region Get topup details in request
            request.setTransactionID(Integer.valueOf(parameter.getTransactionId()));
            request.setTransactionType(TransactionType.COMPLETE_TOPUP);
            request.setProductId(parameter.getProductCode());
            request.setCustomerMobileNumber(parameter.getCustomerMobileNumber());
            request.setTransactionAmount(parameter.getAmount().toString());

            if(!parameter.getLandline().isEmpty() && !parameter.getLandline().equals("0")){
                request.setCustomerTelephoneNo(parameter.getLandline());
            }else if(!parameter.getServiceId().isEmpty() && !parameter.getServiceId().equals("0")){
                request.setCustomerTelephoneNo(parameter.getServiceId());
            }else if (!parameter.getAccountNo().isEmpty() && !parameter.getAccountNo().equals("0")){
                request.setCustomerTelephoneNo(parameter.getAccountNo());
            }
            //#endregion
        }
        request.setPartnerRefNo(parameter.getPartnerRefNo());

        //http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getItemNumberByKitId?kitId={kitId}&itemId={itemId}
        log.debug( "----------[completeTopupExternal] GETITEMNUMBERBYKITID...--------------" );
        log.debug( "----------kitId: " + parameter.getTmnCenterId() + " || itemId:2--------------" );
        String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETITEMNUMBERBYKITID;
        Map< String, Object > params = new HashMap< String, Object >( );
        params.put( "kitId", parameter.getTmnCenterId() );
        params.put( "itemId", "2" );
        String respString = ApiClient.getForObject( uri, String.class, params );
        EndpointResult< String > terminalIdResult = convertCoreResponse( respString );
        if ( !terminalIdResult.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[completeTopupExternal] GETITEMNUMBERBYKITID not found.--------------" );
            response.setResponseCodeMessage( "22" );
            return response;
        }
        request.setTerminalId( terminalIdResult.getData( ) );

        EndpointResult< String > walletIdResponse = getWalletIDFromCore( parameter.getTrueMoneyId( ),
                parameter.getTmnCenterId() );
        if ( !walletIdResponse.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            response.setResponseCodeMessage( "22" );
            return response;
        }
        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getCardNumber?walletId={walletId}";
        log.debug( "----------[completeTopupExternal] GETCARDNUMBER...--------------" );
        log.debug( "----------walletId:" + walletIdResponse.getData( ) + "--------------" );
        String uriAC = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETCARDNUMBER;
        params.clear( );
        params.put( "walletId", walletIdResponse.getData( ) );
        String respStringWalletId = ApiClient.getForObject( uriAC, String.class, params );
        EndpointResult< String > agentCardResult = convertCoreResponse( respStringWalletId );
        if ( !agentCardResult.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[completeTopupExternal] GETCARDNUMBER not found.--------------" );
            response.setResponseCodeMessage( "22" );
            return response;
        }
        request.setAgentCardNumber( agentCardResult.getData( ) );
        request.setSourceCardNumber( "" );
        request.setChannelTypeId( parameter.getChannelTypeId( ) );

        //#region  AdditionalInfo4
        String additionalInformation4 = StringUtils.EMPTY;
        if(!parameter.getAdditionalInformation1().isEmpty() || !parameter.getAdditionalInformation2().isEmpty() ||
                !parameter.getAdditionalInformation3().isEmpty()){
            Map<String ,String > adtnlInfoDictionary = new HashMap<>();
            adtnlInfoDictionary.put("AdditionalInformation1",parameter.getAdditionalInformation1());
            adtnlInfoDictionary.put("AdditionalInformation2",parameter.getAdditionalInformation2());
            adtnlInfoDictionary.put("AdditionalInformation3",parameter.getAdditionalInformation3());
            additionalInformation4 = MapperUtil.objectToJson(adtnlInfoDictionary);
        }
        request.setAdditionalInformation4(additionalInformation4);
        //#endregion

        // CALL completeTopup
        EndpointResult< Map< String, String > > initResponse = completeTopUp( request );
        CompleteTopupResponse initResp = new CompleteTopupResponse( );
        Map< String, String > respData = new HashMap< String, String >( );

        if ( initResponse.getData( ) instanceof Map ) {
            respData = initResponse.getData( );
        }

        Long transId = 0L;
        log.debug( "----------[completeTopupExternal] after completeTopup--------------" );
        log.debug( "----------[completeTopupExternal data]" + objectToJson( initResponse ) + "--------------" );
        if ( initResponse.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            log.debug( "----------[completeTopupExternal] completeTopup valid--------------" );
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "MM-dd-YYYY hh:mm:ss a" );
            LocalDateTime dateNow = LocalDateTime.now( );
            String dateTime = dateNow.format( formatter );
            initResp.setTransactionDate( dateTime );
            initResp.setTransactionId( respData.get( "TransactionId" ) );
            initResp.setAmount( checkTransAmountLength( request.getTransactionAmount() ) );
            initResp.setFee( respData.get( "CashoutFee" ) );
            initResp.setCommission( respData.get("CommissionAmount"));
            initResp.setDiscount( "0" );
            initResp.setOldBalance(respData.get("OldBalance"));
            initResp.setNewBalance(respData.get("NewBalance"));

            log.info( "respData reversalMessage: " + respData.get( "HasReversalMessage" ) );
            response.addData( initResp );
            response.setResponseCode("0");
        } else {
            log.debug( "----------[initiateBillsPaymentV2] initiateBillingV2 error--------------" );
            response.setData( null );
            response.setResponseCodeMessage( initResponse.getResponseCode( ) );
            response.setResponseMessage( initResponse.getResponseMessage( ) );
        }

        return response;
    }


    private Integer validateIfDuplicateTransaction( Integer completetransType, String transactionId ) {
        log.debug( "----------[validateIfDuplicateTransaction start]--------------" );
        Integer ret = 0;
        try {
            //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/validateIfDuplicateTransaction?transactionTypeId={transactionTypeId}&parentId={parentId}";
            String uriSourceWallet = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETVALIDATEDUPLICATETRANSACTION;
            Map< String, Object > params = new HashMap<>( );
            params.put( "transactionTypeId", completetransType );
            params.put( "parentId", transactionId );
            String respString = ApiClient.getForObject( uriSourceWallet, String.class, params );
            log.debug( "----------[validateIfDuplicateTransaction response]" + respString + "--------------" );
            EndpointResult< String > result = convertCoreResponse( respString );
//			ret = Integer.parseInt(result.getData());
            // TODO: verify response of validateIfDuplicateTransaction
            ret = Integer.parseInt( result.getResponseCode( ) );
            log.debug( "----------[validateIfDuplicateTransaction end]--------------" );
            return ret;
        } catch ( Exception e ) {
            e.printStackTrace( );
            log.debug( "----------[validateIfDuplicateTransaction excepiton]--------------" );
            return ret;
        }
    }

    private Topup n3ToEdcTopup(TopupN3 request) {
        Topup topupRequest = new Topup();

        topupRequest.setTransactionType(request.getTransactionType());
        topupRequest.setServiceProductId(request.getServiceProductId());
        topupRequest.setProductId(request.getProductId());
        topupRequest.setTransactionAmount(request.getTransactionAmount());
        topupRequest.setCustomerMobileNumber(request.getCustomerMobileNumber());
        topupRequest.setCustomerTelephoneNo(request.getCustomerTelephoneNo());
        topupRequest.setAgentCardNumber(request.getAgentCardNumber());
        topupRequest.setTerminalId(request.getTerminalId());
        topupRequest.setTransactionID(request.getTransactionID());
        topupRequest.setSourceCardNumber(request.getSourceCardNumber());
        topupRequest.setChannelTypeId(request.getChannelTypeId());
        topupRequest.setPromoWalletId(request.getPromoWalletId());

        return topupRequest;
    }

    private String checkTransAmountLength( String amount ) {

        String num = amount;
        int indexOf = num.indexOf( "." );
        log.info( "decimal: " + num.substring( indexOf ) );
        int add = 0;
        if ( num.substring( indexOf ).length( ) < 4 ) {
            log.info( "less than 4" );
            add = 4 - ( num.substring( indexOf ).length( ) - 1 );
            log.info( "add: " + add );
        }
        String numPadded = StringUtils.rightPad( num, num.length( ) + add, "0" );
        log.info( "numpadded: " + numPadded );

        return numPadded;
    }

    private EndpointResult getWalletIDFromCore( Long trueMoneyId, Long TMNCenterId ) {
        log.debug( "----------DEBUG START GET WALLET ID FROM CORE--------------" );
        log.debug( "----------TrueMoneyId: " + trueMoneyId + " || TMNCenterId: " + TMNCenterId + "--------------" );
        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getWalletIdByPartnerIdKitId?PartnerId={PartnerId}&kitId={kitId}";
        String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETWALLETIDBYKITID;

        Map< String, Object > params = new HashMap< String, Object >( );
        params.put( "PartnerId", trueMoneyId );
        params.put( "kitId", TMNCenterId );

        String respString = ApiClient.getForObject( uri, String.class, params );
        log.debug( "----------[get wallet id by kit id]" + respString + "--------------" );
        EndpointResult< String > walletIdResult = convertCoreResponse( respString );
        log.debug( "----------DEBUG END GET WALLET ID FROM CORE--------------" );
        return walletIdResult;
    }

    private EndpointResult insertServiceTransaction( Long serviceCategoryId, Topup request,
                                                     String transactionId, Integer createdBy, Long serviceProviderId ) {
        String uriInsertTransaction = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTINSERTSERVICETRANSACTION;
        Map< String, Object > params = new HashMap< String, Object >( );
        EndpointResult insertTransResp = new EndpointResult( );
        params.put( "type", serviceCategoryId );
        params.put( "request", objectToJson( request ) );
        params.put( "transactionID", Integer.parseInt( transactionId ) );
        params.put( "createdBy", createdBy );
        params.put( "categoryId", serviceCategoryId );
        params.put( "serviceProviderId", serviceProviderId );
        if ( !transactionId.isEmpty( ) ) {
            HttpEntity< MultiValueMap< String, String > > insertRequest = new HttpEntity( params );
            String respString = ApiClient.postForObject( uriInsertTransaction, insertRequest, String.class );
            insertTransResp = convertCoreResponse( respString );
        }
        return insertTransResp;
    }

    private EndpointResult updateServiceTransaction( String transactionId, Object response, Topup request,
                                                     String customerMobileNumber, String rrn, int tag, ThirdPartyResult thirdPartyResult ) {

//		Map<String, String> responseTpRes = new HashMap<>();
//		responseTpRes.put("RRN","");
//		responseTpRes.put("RESP",thirdPartyResult.getRESP());
//		responseTpRes.put("TID","");
//		responseTpRes.put("BAL","");
//		responseTpRes.put("EPIN","");
//		responseTpRes.put("ERR",thirdPartyResult.getERR());
//		responseTpRes.put("ORESP",thirdPartyResult.getORESP());
//		responseTpRes.put("OERR",thirdPartyResult.getOERR());
//		responseTpRes.put("REFNUMBER","");

        ServiceTransactionDTO serviceTransactionDTO = null;
        if ( thirdPartyResult != null ) {
            serviceTransactionDTO = new ServiceTransactionDTO( );
            serviceTransactionDTO.setRrn( thirdPartyResult.getRRN( ) );
            serviceTransactionDTO.setResp( thirdPartyResult.getRESP( ) );
            serviceTransactionDTO.setTid( thirdPartyResult.getTID( ) );
            serviceTransactionDTO.setBal( thirdPartyResult.getBAL( ) );
            serviceTransactionDTO.setEpin( thirdPartyResult.getEPIN( ) );
            serviceTransactionDTO.setErr( thirdPartyResult.getERR( ) );
            serviceTransactionDTO.setOresp( thirdPartyResult.getORESP( ) );
            serviceTransactionDTO.setOerr( thirdPartyResult.getOERR( ) );
            serviceTransactionDTO.setRefNumber( thirdPartyResult.getREFNUMBER( ) );
        }

        EndpointResult< String > result = new EndpointResult( );
        try {
            //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/updateServiceTransaction";
            String uriUpdateTransaction = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTUPDATESERVICETRANSACTION;
            Map< String, Object > params = new HashMap< String, Object >( );
            params.put( "transactionId", transactionId );
            params.put( "response", objectToJson( response ) );
            params.put( "request", objectToJson( request ) );
            params.put( "customerMobileNumber", customerMobileNumber );
            params.put( "rrn", rrn );
            params.put( "tag", tag );
            params.put( "responseTMNFormat", serviceTransactionDTO == null ? "" : objectToJson( serviceTransactionDTO ) );

            HttpEntity< MultiValueMap< String, String > > updateRequest = new HttpEntity( params );
            String respUpdateString = ApiClient.postForObject( uriUpdateTransaction, updateRequest, String.class );
            result = convertCoreResponse( respUpdateString );
        } catch ( Exception e ) {
            log.error( e.getMessage( ) );
        }
        return result;
    }

    private EndpointResult processTransaction( Long initiatorWalletId, Long sourceWalletId, Long targetWalletId,
                                               String transactionAmount, String terminalId, Integer transactionType, String channelId,
                                               String transactionId, String referenceNumber, String initiatorcardNumber, String additionalInfo1,
                                               String additionalInfo2, String additionalInfo3, String promoCode, String mobileNumber,
                                               String senderMobileNumber, String receiverMobileNumber, Double billerFee, int tmnCenterId,
                                               String partnerReferenceNumber, String additionalInfo4, String status, String response ) {

        log.debug( "----------DEBUG START processTransaction--------------" );
        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/processTransaction";
        String uriPTNew = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.POSTPROCESSTRANSACTION;

        Map< String, Object > params = new HashMap<>( );
        params.put( "initiatorWalletId", initiatorWalletId );
        params.put( "sourceWalletId", sourceWalletId );
        params.put( "targetWalletId", targetWalletId );
        params.put( "transactionAmount", transactionAmount );
        params.put( "terminalId", terminalId );
        params.put( "transactionType", transactionType );
        params.put( "channelId", channelId );
        params.put( "transactionId", transactionId );
        params.put( "referenceNumber", referenceNumber );
        params.put( "initiatorcardNumber", initiatorcardNumber );
        params.put( "additionalInfo1", additionalInfo1 );
        params.put( "additionalInfo2", additionalInfo2 );
        params.put( "additionalInfo3", additionalInfo3 );
        params.put( "promoCode", promoCode );
        params.put( "mobileNumber", mobileNumber );
        params.put( "senderMobileNumber", senderMobileNumber );
        params.put( "receiverMobileNumber", receiverMobileNumber );
        params.put( "billerFee", Double.valueOf( billerFee ) );
        params.put( "tmnCenterId", tmnCenterId );
        params.put( "partnerReferenceNumber", partnerReferenceNumber );
        params.put( "additionalInfo4", additionalInfo4 );
        params.put( "responseCode", status );
        params.put( "responseMessage", response );
        log.debug( "----------[processTransaction params]" + objectToJson( params ) + "--------------" );
        HttpEntity< MultiValueMap< String, String > > ptRequest = new HttpEntity( params );
        String stringProcessTrans = ApiClient.postForObject( uriPTNew, ptRequest, String.class );
        log.debug( "----------[processTransaction response]" + stringProcessTrans + "--------------" );
        // ApiClient.getForObject(uriPTNew, String.class, params);
        // postForObject(uriPTNew, spiRequest, EndpointResult.class);
        EndpointResult processTransResult = convertCoreResponse( stringProcessTrans );
        log.debug( "----------DEBUG END processTransaction--------------" );
        return processTransResult; // TODO: format the Transaction object based from the EndpointResult
    }

    private Map<String, String> validateProductCode(String productId, String transactionAmount) {
        log.debug( "----------validateProductCode start--------------" );
        Map<String,String> validateResponse = new HashMap<>();

        PostTransactionDetailsDTO transDetailsDTO = new PostTransactionDetailsDTO( );

        transDetailsDTO =
                repoService.getProductDetailsByProductCode( productId );
        log.debug( "----------[validation object]" + objectToJson( transDetailsDTO ) + "--------------" );

        transDetailsDTO.setResultCode(transDetailsDTO != null ? "0" : "error");

        // return validationResult
        validateResponse.put( "validationResult", objectToJson( transDetailsDTO ) );

        return validateResponse;
    }

    private Long getSourceWalletId( Topup request ) {
        log.debug( "----------[getSourceWalletId start]--------------" );
        log.debug( "----------[getSourceWalletId]" + objectToJson( request ) + "--------------" );
        Long sourceWalletId = 0L;
        String sourceCardNumber = "";

        if ( request.getTransactionType() == TransactionType.INITIATE_TOPUP_T ||
                request.getTransactionType() == TransactionType.COMPLETE_TOPUP_T )
            sourceCardNumber = request.getSourceCardNumber( );
        else if (request.getTransactionType() == TransactionType.TOPUP_REWARD_BILLING ||
                request.getTransactionType() == TransactionType.TOPUP_REWARD_SENDER ||
                request.getTransactionType() == TransactionType.TOPUP_REWARD_RECEIVER)
            sourceCardNumber = request.getPromoWalletId( ).toString();
        else
            sourceCardNumber = request.getAgentCardNumber( );
        //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/getWalletIdbyCardNumber?cardNumber={cardNumber}";
        String uriSourceWallet = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETWALLETID;
        Map< String, Object > params = new HashMap<>( );
        params.put( "cardNumber", sourceCardNumber );
        String respStringWalletId = ApiClient.getForObject( uriSourceWallet, String.class, params );
        log.debug( "----------[getSourceWalletId core response]" + respStringWalletId + "--------------" );
        EndpointResult< String > sourceWalletResult = convertCoreResponse( respStringWalletId );
        if ( sourceWalletResult.getResponseCode( ).equalsIgnoreCase( "0" ) ) {
            sourceWalletId = Long.valueOf( sourceWalletResult.getData( ) );
        }

        log.debug( "----------[getSourceWalletId end]--------------" );

        return sourceWalletId;
    }

//    private Long getTargetWalletId( String productCode ) {
//        log.debug( "----------[getTargetWalletId start]--------------" );
//        productCode = productCode.toUpperCase( );
//        log.debug( "----------[prod_code]" + productCode + "--------------" );
//        if ( productCode.equalsIgnoreCase( ServiceCode.ILOILOSALES ) )
//            return Wallets.ILOILOSALES;
//
//        log.debug( "----------[getTargetWalletId end]--------------" );
//        return Wallets.PALOAD;
//    }

    private Boolean validateMobileNumber( Integer transactionTypeId, Topup request ) {
        log.debug( "----------DEBUG START MOBILE NUMBER VALIDATE--------------" );
        Boolean ret = true;

        if ( !request.getCustomerMobileNumber( ).isEmpty( ) ) {
            log.debug( "----------mobile number not empty--------------" );
            log.debug( "----------params: transactionTypeId: " + transactionTypeId +
                    " || mobileNumber: " + request.getCustomerMobileNumber( ) + "--------------" );
            //"http://10.14.20.171:82/Core/Java/Java.svc/RESTful/checkBlacklistMobileNumber";
            String uri = TopupConstants.ADDRS.COREADDR + TopupConstants.URIS.GETBLACKLISTMOBILENUMBER;
            Map< String, Object > validateReqBody = new HashMap< String, Object >( );
            validateReqBody.put( "transactionTypeId", transactionTypeId );
            validateReqBody.put( "mobileNumber", request.getCustomerMobileNumber( ) );
            validateReqBody.put( "serialNumber", StringUtils.EMPTY );

            HttpHeaders headers = new HttpHeaders( );
            headers.setContentType( APPLICATION_JSON_UTF8 );
            HttpEntity<MultiValueMap< String, String >> spiRequest = new HttpEntity( validateReqBody, headers );

            String respString = ApiClient.postForObject( uri, spiRequest, String.class );
            EndpointResult respObject = convertCoreResponse( respString );
            log.debug( "----------[mobile validation response]" + respString + "--------------" );
            if ( !respObject.getResponseCode( ).equalsIgnoreCase( "0" ) )
                ret = false;
        }

        log.debug( "----------DEBUG END MOBILE NUMBER VALIDATE--------------" );
        return ret;
    }

    private EndpointResult convertCoreResponse( String responseString ) {
        log.debug( "----------DEBUG START convertCoreResponse--------------" );
        String resultString = jsonToObject( responseString, String.class );
        log.info( "convertCoreResponse resultString: " + resultString );
        Map< String, Object > resultMap = jsonToObject( resultString, Map.class );

        String responseMessage = StringUtils.isEmpty(String.valueOf( resultMap.get( "ResponseMessage" )))?String.valueOf( resultMap.get( "transaction_message" )):String.valueOf( resultMap.get( "ResponseMessage" ));

        log.info("responseMessage: "+ responseMessage);

        EndpointResult result = new EndpointResult( )
//                .addError( (String) resultMap.get( "ResponseCode" ) )
                .setResponseCode( String.valueOf( resultMap.get( "ResponseCode" ) ) )
                .setResponseMessage( responseMessage )
                .addData( resultMap.getOrDefault( "data", resultMap.get( "Data" ) ) );

        log.debug( "----------DEBUG END convertCoreResponse--------------" );
        return result;
    }

    @Override
    public String convertMobileNumber(String mobileNumber) {
        String reqMobileNumber = MobileNumberUtil
                .convertNumberIntl( MobileNumberUtil.convertNumberToLocal( mobileNumber ) );
        return reqMobileNumber;
    }

}
