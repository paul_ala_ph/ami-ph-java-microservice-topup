package ph.com.truemoney.topup.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class PortalEndpointResult<T> {
    @JsonProperty("ResponseCode")
    String transactionResponseCode;
    @JsonProperty("ResponseMessage")
    String transactionMessage;
    @JsonProperty("Data")
    T data;
}