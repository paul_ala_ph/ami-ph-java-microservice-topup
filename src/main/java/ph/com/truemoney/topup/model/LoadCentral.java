package ph.com.truemoney.topup.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
public class LoadCentral {
    private String RRN;
    private String RESP;
    private String TID;
    private String BAL;
    private String EPIN;
    private String ERR;
    private String ORESP;
    private String OERR;

    @JsonProperty( "RRN" )
    public String getRRN( ) {
        return RRN == null ? "" : RRN;
    }

    @JsonProperty( "RESP" )
    public String getRESP( ) {

        return RESP == null ? "" : RESP;
    }

    @JsonProperty( "TID" )
    public String getTID( ) {
        return TID == null ? "" : TID;
    }

    @JsonProperty( "BAL" )
    public String getBAL( ) {
        return BAL == null ? "" : BAL;
    }

    @JsonProperty( "EPIN" )
    public String getEPIN( ) {
        return EPIN == null ? "" : EPIN;
    }

    @JsonProperty( "ERR" )
    public String getERR( ) {
        return ERR == null ? "" : ERR;
    }

    @JsonProperty( "ORESP" )
    public String getORESP( ) {
        return ORESP == null ? "" : ORESP;
    }

    @JsonProperty( "OERR" )
    public String getOERR( ) {
        return OERR == null ? "" : OERR;
    }


}
