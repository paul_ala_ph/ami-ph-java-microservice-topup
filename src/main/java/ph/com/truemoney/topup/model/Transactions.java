package ph.com.truemoney.topup.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming( PropertyNamingStrategy.UpperCamelCaseStrategy.class )
public class Transactions {
	private String transactionId;
    private String transactionAmount;
    private String transactionDateTime;
    private String transactionType;
    private String transactionStatus;
    private String referenceNumber;
    private String cashoutFee;
    private String responseCode;
    private String remittanceCode;
    private String profileId;
    private String senderMobileNumber;
    private String availableBalance;
    @JsonProperty("promoAmount")
    private String promoAmount;
    @JsonProperty("minMaxAmount")
    private String minMaxAmount;
    @JsonProperty("isAWinner")
    private String isAWinner;
    @JsonProperty("rewardAmount")
    private String rewardAmount;
    private String rewardsPointEarned;
    private String previousBalance;;
    private String discount;
    private String totalAmount;
    @JsonProperty("TMNCenterId")
    private String tmnCenterId;
    private String maintainingBalance;
//    private String commission;
//    private String commissionAmount;
//    private String otherFee;
//    private String oldBalance;
//    private String newBalance;
    private String sourceNewBalance;
    private String additionalPromoAmount;
    private String commissionAmount;
    private String sourceOldBalance;
    
    
    public Transactions() {
    	transactionId = "0";
        transactionAmount = "0.00";
        transactionType = "";
        transactionStatus = "";
        referenceNumber = "";
        cashoutFee = "0.00";
        responseCode = "";
        remittanceCode = "";
        profileId = "";
        senderMobileNumber = "";
        promoAmount = "0.00";
        availableBalance = "0.00";
        minMaxAmount = "0.00";
        isAWinner = "0";
        rewardAmount = "0.00";
        discount = "0.00";
        totalAmount = "0.00";
        tmnCenterId = "";
        commissionAmount = "0.00";
    }
}
