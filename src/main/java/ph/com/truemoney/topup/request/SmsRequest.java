package ph.com.truemoney.topup.request;

import lombok.Data;

import java.util.Map;

@Data
public class SmsRequest {
	Integer notifType = 2;
	Map<String, String> params;
	String to;
	String type;
}
