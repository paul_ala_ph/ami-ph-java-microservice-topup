package ph.com.truemoney.topup.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;


@Data
@JsonNaming( PropertyNamingStrategy.UpperCamelCaseStrategy.class )
public class CompleteTopupRequest {
    private Long trueMoneyId = 0L;
	private String token = StringUtils.EMPTY;

	@JsonProperty(value="TMNCenterId")
    private Long tmnCenterId = 0L;
    private Integer channelTypeId = 12;
    private String TransactionId = "0";
    private String customerMobileNumber = StringUtils.EMPTY;
    private String productCode = StringUtils.EMPTY;
    private Double amount = 0.0;
    private String promoCode = StringUtils.EMPTY;

    private String landline = StringUtils.EMPTY;
    private String serviceId = StringUtils.EMPTY;
    private String accountNo = StringUtils.EMPTY;
    private String partnerRefNo = StringUtils.EMPTY;
    private String additionalInformation1 = StringUtils.EMPTY;
    private String additionalInformation2 = StringUtils.EMPTY;
    private String additionalInformation3 = StringUtils.EMPTY;

}
