package ph.com.truemoney.topup.request;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;


@Data
@JsonNaming( PropertyNamingStrategy.UpperCamelCaseStrategy.class )
public class TopupN3 {
    private Integer transactionType = 0;
	private String productId = StringUtils.EMPTY;
    private String serviceProductId = StringUtils.EMPTY;
    private String transactionAmount = StringUtils.EMPTY;
    private String customerMobileNumber = StringUtils.EMPTY;
    private String customerTelephoneNo = StringUtils.EMPTY;
    private String agentCardNumber = StringUtils.EMPTY;
    private String terminalId = StringUtils.EMPTY;
    private Integer transactionID = 0;
    private String sourceCardNumber = StringUtils.EMPTY;
    private Integer channelTypeId = 18;
    private Long promoWalletId = 0L;
}
