package ph.com.truemoney.topup.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ph.com.truemoney.base.response.EndpointResult;
import ph.com.truemoney.topup.dto.PostTransactionDetailsDTO;
import ph.com.truemoney.topup.model.PortalEndpointResult;
import ph.com.truemoney.topup.request.CompleteTopupRequest;
import ph.com.truemoney.topup.request.InitiateTopupRequest;
import ph.com.truemoney.topup.request.Topup;
import ph.com.truemoney.topup.request.TopupN3;
import ph.com.truemoney.topup.services.RepoService;
import ph.com.truemoney.topup.services.TopupService;
import ph.com.truemoney.utils.MapperUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static ph.com.truemoney.utils.MapperUtil.objectToJson;

/**
 * Topup controller.
 */
@Log4j2
@RestController
@RequestMapping( value = "/topup" )
public class TopupController {
	@Autowired
    private TopupService topupService;

	@Autowired
	private RepoService repoService;


    @ApiOperation( value = "This API processes the initialization of Topup for EDC." )
    @PostMapping(value = "edc/InitiateTopUp")
    public String initiateTopUpEDC(@RequestBody Topup request) {
    	log.debug("----------DEBUG START initiateTopUp Controller for EDC--------------");
		log.debug("request:"+request);

        EndpointResult initiateTopupResponse = new EndpointResult( );
        PortalEndpointResult response = new PortalEndpointResult( );
        try {
            initiateTopupResponse = topupService.initiateTopup( request );
            response.setTransactionResponseCode( initiateTopupResponse.getResponseCode( ) );
            response.setData( initiateTopupResponse.getData( ) );
            response.setTransactionMessage( initiateTopupResponse.getResponseMessage( ) );
        }catch (Exception e ){
            initiateTopupResponse.setResponseCode( "22" );
            log.error( e.getMessage( ) + " " + e.getCause( ) );
        }

        try {
            return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
        } catch ( JsonProcessingException e ) {
            log.error( e.getMessage( ) );
        }

		return "Error returning result.";
    }

    @ApiOperation( value = "This API processes the initialization of Topup for External." )
	@PostMapping(value = "external/InitiateTopUp")
	public String initiateTopUpExternal(
			@RequestBody InitiateTopupRequest request) {
		log.debug("----------DEBUG START initiateTopUp Controller for External--------------");
		log.debug("request:"+request);

		EndpointResult initiateTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );
		try {
			initiateTopupResponse = topupService.initiateTopupExternal( request );
			response.setTransactionResponseCode( initiateTopupResponse.getResponseCode( ) );
			response.setData( initiateTopupResponse.getData( ) );
			response.setTransactionMessage( initiateTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			initiateTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}

    @ApiOperation( value = "This API processes the initialization of Topup for N3." )
	@PostMapping(value = "n3/InitiateTopUp")
	public String initiateTopUpN3(
			@RequestBody TopupN3 request) {
		log.debug("----------DEBUG START initiateTopUp Controller for N3--------------");
		log.debug("request:"+request);

		EndpointResult initiateTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );
		try {
			initiateTopupResponse = topupService.initiateTopupN3( request );
			response.setTransactionResponseCode( initiateTopupResponse.getResponseCode( ) );
			response.setData( initiateTopupResponse.getData( ) );
			response.setTransactionMessage( initiateTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			initiateTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}

    @ApiOperation( value = "This API processes the initialization of Topup for Phone." )
	@PostMapping(value = "phone/InitiateTopUp")
	public String initiateTopUpPhone(
			@RequestBody Topup request) {
		log.debug("----------DEBUG START initiateTopUp Controller for Phone--------------");
		log.debug("request:"+request);

		EndpointResult initiateTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );

		try {
			initiateTopupResponse = topupService.initiateTopupPhone( request );
			response.setTransactionResponseCode( initiateTopupResponse.getResponseCode( ) );
			response.setData( initiateTopupResponse.getData( ) );
			response.setTransactionMessage( initiateTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			initiateTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}





	@ApiOperation( value = "This API processes the completion of Topup for EDC." )
	@PostMapping(value = "edc/completeTopUp")
	public String completeTopUpEDC(@RequestBody Topup request) {
		log.debug("----------DEBUG START completeTopUp Controller for EDC--------------");
		log.debug("request:"+request);

		EndpointResult completeTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );
		try {
			completeTopupResponse = topupService.completeTopUp( request );
			response.setTransactionResponseCode( completeTopupResponse.getResponseCode( ) );
			response.setData( completeTopupResponse.getData( ) );
			response.setTransactionMessage( completeTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			completeTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}


	@ApiOperation( value = "This API processes the completion of Topup for External." )
	@PostMapping(value = "external/CompleteTopUp")
	public String completeTopUpExternal(
			@RequestBody CompleteTopupRequest request) {
		log.debug("----------DEBUG START completeTopUp Controller for External--------------");
		log.debug("request:"+request);

		EndpointResult completeTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );
		try {
			completeTopupResponse = topupService.completeTopupExternal( request );
			response.setTransactionResponseCode( completeTopupResponse.getResponseCode( ) );
			response.setData( completeTopupResponse.getData( ) );
			response.setTransactionMessage( completeTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			completeTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}


	@ApiOperation( value = "This API processes the completion of Topup for N3." )
	@PostMapping(value = "n3/completeTopUp")
	public String completeTopUpN3(
			@RequestBody TopupN3 request) {
		log.debug("----------DEBUG START completeTopUp Controller for N3--------------");
		log.debug("request:"+request);

		EndpointResult completeTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );
		try {
			completeTopupResponse = topupService.completeTopUpN3( request );
			response.setTransactionResponseCode( completeTopupResponse.getResponseCode( ) );
			response.setData( completeTopupResponse.getData( ) );
			response.setTransactionMessage( completeTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			completeTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}


	@ApiOperation( value = "This API processes the completion of Topup for Phone." )
	@PostMapping(value = "phone/CompleteTopUp")
	public String completeTopUpPhone(
			@RequestBody Topup request) {
		log.debug("----------DEBUG START completeTopUp Controller for Phone--------------");
		log.debug("request:"+request);

		EndpointResult completeTopupResponse = new EndpointResult( );
		PortalEndpointResult response = new PortalEndpointResult( );

		try {
			completeTopupResponse = topupService.completeTopupPhone( request );
			response.setTransactionResponseCode( completeTopupResponse.getResponseCode( ) );
			response.setData( completeTopupResponse.getData( ) );
			response.setTransactionMessage( completeTopupResponse.getResponseMessage( ) );
		}catch (Exception e ){
			completeTopupResponse.setResponseCode( "22" );
			log.error( e.getMessage( ) + " " + e.getCause( ) );
		}

		try {
			return new ObjectMapper( ).writeValueAsString( objectToJson( response ) );
		} catch ( JsonProcessingException e ) {
			log.error( e.getMessage( ) );
		}

		return "Error returning result.";
	}


//	@ApiOperation( value = "This is a test endpoint for reposervice getProductDetailsByProductCode." )
//	@PostMapping(value = "test/GetProductDetailsByProductCode")
//	public String getProductDetailsByProductCode(
//			@RequestBody String request) {
//
//		PostTransactionDetailsDTO transDetailsDTO = new PostTransactionDetailsDTO( );
//
//		transDetailsDTO =
//				repoService.getProductDetailsByProductCode( request );
//		log.debug( "----------[validation object]" + objectToJson( transDetailsDTO ) + "--------------" );
//
//		return "Error returning result.";
//	}
}
