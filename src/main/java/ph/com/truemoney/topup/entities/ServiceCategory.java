package ph.com.truemoney.topup.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import ph.com.truemoney.base.data.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Data
@Table(name = "L_ServiceCategory")
@Entity
@AttributeOverride( name = "id", column = @Column( name = "ServiceCategoryId", columnDefinition = "bigint") )
public class ServiceCategory extends BaseEntity {
	private String serviceCategory;
	private Long parentId;
	private Boolean isParent = false;
	
	@JsonIgnore
    @OneToMany(mappedBy = "serviceCategory")
    private Set<ServiceEntry> servicesList;
}
