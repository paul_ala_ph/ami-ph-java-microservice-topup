package ph.com.truemoney.topup.entities;

import lombok.Data;
import ph.com.truemoney.base.data.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Table(name = "L_ServiceProductField")
@Entity
@AttributeOverride( name = "id", column = @Column( name = "ServiceFieldId", columnDefinition = "bigint") )
public class ServiceProductField extends BaseEntity {
	@ManyToOne
    @JoinColumn(name = "ServiceProductId")
    private ServiceProduct serviceProduct;
	
	private String fieldName;
	private String fieldLabel;
	private String fieldDescription;
	private Integer fieldWidth;
    private String fieldNote;
    private Integer fieldOrder;
    private String fieldType;
    private String fieldValueTag;
    private String fieldTargetName;
    private Boolean amountRule;
    private Boolean isRequired;
    private String mask;
    private String padding;
    private Long modifiedBy;
    private LocalDateTime modifiedTime;
}
