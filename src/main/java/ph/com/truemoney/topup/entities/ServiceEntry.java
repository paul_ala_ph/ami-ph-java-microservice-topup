package ph.com.truemoney.topup.entities;

import lombok.Data;
import ph.com.truemoney.base.data.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Table(name = "L_Service")
@Entity
@AttributeOverride( name = "id", column = @Column( name = "ServiceId", columnDefinition = "bigint") )
public class ServiceEntry extends BaseEntity {
	private String productCode;
	private String productDescription;
	private Integer category;
	private Boolean isActive;
	private Long modifiedBy;
	private LocalDateTime modifiedTime;
	private Integer merchantId;
	private Double amount;
	private Long displayCategoryTypeId;
	private Boolean hasValidation;
	private Boolean hasSmartPricing;
	private Long insuranceCategoryId;
	
	@ManyToOne
    @JoinColumn(name = "ServiceCategoryId")
	private ServiceCategory serviceCategory;
	
	@OneToMany(mappedBy = "service")
    private Set<ServiceProduct> serviceProductList;
	
	//TODO: change to double if needed
	private String convenienceFee;
	private String productAcronym;
	private Integer settlementScheduleId;
	private Boolean hasPaymentCorrection;
	private Boolean hasInsuranceAdvertisement;
	private Boolean hasReversalMessage;
	private String accountFormat;
}
