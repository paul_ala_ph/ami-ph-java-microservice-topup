package ph.com.truemoney.topup.entities;

import lombok.Data;
import ph.com.truemoney.base.data.BaseEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Table(name = "L_ServiceProduct")
@Entity
@AttributeOverride( name = "id", column = @Column( name = "ServiceProductId", columnDefinition = "bigint") )
public class ServiceProduct extends BaseEntity {
	@OneToMany
    private List<ServiceProductField> serviceProductFieldList;
	
	private String serviceProductCode;
	
	@ManyToOne
    @JoinColumn(name = "ServiceId")
    private ServiceEntry service;
	
	@ManyToOne
    @JoinColumn(name = "ServiceProviderId")
    private ServiceProvider serviceProvider;
	
	private Double sellingAmount;
	private Integer priority;
	private Boolean isActive;
	private Boolean amountRule;
	private Double minimumAmount;
	private Long modifiedBy;
	private LocalDateTime modifiedTime;
	private String keys1;
	private String keys2;
	private String keys3;
	private String keys4;
	private Boolean withAmountField;
	private String amountFieldNote;
	
	private String name;
	private String description;
	private Double serviceFee;
	private String firstFieldLabel;
	private String firstFieldDescription;
	private String firstFieldWidth;
	private String firstFieldNote;
	private String secondFieldLabel;
	private String secondFieldDescription;
	private String secondFieldWidth;
	private String secondFieldNote;
	private Boolean acceptsPastDue;
	private Boolean acceptsOverPayment;
	private Boolean acceptsUnderPayment;
}
