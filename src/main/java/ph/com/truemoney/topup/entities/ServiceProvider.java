package ph.com.truemoney.topup.entities;

import lombok.Data;
import ph.com.truemoney.base.data.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Data
@Table(name = "L_ServiceProvider")
@Entity
@AttributeOverride( name = "id", column = @Column( name = "ServiceProviderId", columnDefinition = "bigint") )
public class ServiceProvider extends BaseEntity {
	private String name;
	private Boolean isActive;
	private String service;
	
	@OneToMany(mappedBy = "serviceProvider")
    private Set<ServiceProduct> serviceProductsList;
}
