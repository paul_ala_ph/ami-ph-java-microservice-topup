package ph.com.truemoney.topup.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ServiceTransactionDTO {

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getBal() {
        return bal;
    }

    public void setBal(String bal) {
        this.bal = bal;
    }

    public String getEpin() {
        return epin;
    }

    public void setEpin(String epin) {
        this.epin = epin;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getOresp() {
        return oresp;
    }

    public void setOresp(String oresp) {
        this.oresp = oresp;
    }

    public String getOerr() {
        return oerr;
    }

    public void setOerr(String oerr) {
        this.oerr = oerr;
    }

    public String getRefNumber() {
        return refNumber;
    }

    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    @JsonProperty("RRN")
    private String rrn;
    @JsonProperty("RESP")
    private String resp;
    @JsonProperty("TID")
    private String tid;
    @JsonProperty("BAL")
    private String bal;
    @JsonProperty("EPIN")
    private String epin;
    @JsonProperty("ERR")
    private String err;
    @JsonProperty("ORESP")
    private String oresp;
    @JsonProperty("OERR")
    private String oerr;
    @JsonProperty("REFNUMBER")
    private String refNumber;

}
