package ph.com.truemoney.topup.dto;

import lombok.Data;
import ph.com.truemoney.topup.entities.ServiceEntry;

@Data
public class ServiceDTO {
	private Long serviceId;
    private String productCode;
    private String productDesc;
    private Boolean withAmountField;
    private Boolean amountRule;
    private String amountFieldNote;
    private Long serviceProductId;
    private Long serviceProviderId;
    // not in DB
//    private String FirstFieldLabel;
//    private String FirstFieldDesc;
//    private String FirstFieldWidth;
//    private String FirstFieldNote;
//    private String SecondFieldLabel;
//    private String SecondFieldDesc;
//    private String SecondFieldWidth;
//    private String SecondFieldNote;
    
    //Long ServiceId, String ProductCode, String ProductDesc,
    public ServiceDTO(ServiceEntry s,
                      Boolean WithAmountField, Boolean AmountRule, String AmountFieldNote,
                      Long ServiceProductId, Long ServiceProviderId) {
    	this.serviceId = s.getId();
        this.productCode = s.getProductCode();
        this.productDesc = s.getProductDescription();
        this.withAmountField = WithAmountField;
        this.amountRule = AmountRule;
        this.amountFieldNote = AmountFieldNote;
        this.serviceProductId = ServiceProductId;
        this.serviceProviderId = ServiceProviderId;
    }
    
    
}
