package ph.com.truemoney.topup.dto;

import lombok.Data;

@Data
public class PostTransactionDetailsDTO {
    private Long serviceId;
    private Boolean hasInsuranceAd;
    private Boolean hasReversalMessage;
    private String productCode;
    private String productDescription;
    private Long serviceProviderId;
    private String serviceProviderName;
	private Long serviceProductId;
    private String serviceProductCode;
    private Long serviceCategoryId;
    private Integer category;
    private Boolean hasValidation;
    private String fieldLabel;
    private String resultCode;
    private String keys1;
    private String keys2;
    private String keys3;
    private String keys4;
    private String firstFieldLabel;
    private String secondFieldLabel;
    private Boolean withAmountField;
    private String acronym;
    private Boolean acceptsPastDue;

    public PostTransactionDetailsDTO() {
    }
    
    public PostTransactionDetailsDTO(Long serviceId, Boolean hasInsuranceAd, Boolean hasReversalMessage,
                                     Long serviceCategoryId, String productCode,
                                     Long serviceProductId, Long serviceProviderId,
                                     String serviceProductCode, Integer category,
                                     String keys1, String keys2, String keys3, String keys4,
                                     Boolean withAmountField,
                                     String firstFieldLabel, String secondFieldLabel, Boolean acceptsPastDue) {
    	this.serviceId = serviceId;
    	this.hasInsuranceAd = hasInsuranceAd;
    	this.hasReversalMessage = hasReversalMessage;
    	this.serviceCategoryId = serviceCategoryId;
    	this.productCode = productCode;
        this.serviceProductId = serviceProductId;
        this.serviceProviderId = serviceProviderId;
        this.serviceProductCode = serviceProductCode;
        this.category = category;
        this.keys1 = keys1;
        this.keys2 = keys2;
        this.keys3 = keys3;
        this.keys4 = keys4;
        this.withAmountField = withAmountField;
        this.firstFieldLabel = firstFieldLabel;
        this.secondFieldLabel = secondFieldLabel;
        this.acceptsPastDue = acceptsPastDue;
    }
}
