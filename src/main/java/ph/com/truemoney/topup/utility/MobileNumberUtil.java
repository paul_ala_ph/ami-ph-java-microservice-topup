package ph.com.truemoney.topup.utility;

import org.apache.commons.lang3.StringUtils;

public class MobileNumberUtil {
	public static String convertNumberToLocal(String contactNumber) {
		if (!StringUtils.isEmpty(contactNumber)) {
			if(contactNumber.startsWith("6")) {
				return "0" + contactNumber.substring(2);
			} else {
				return contactNumber;
			}
		} else {
			return contactNumber;
		}
	}
	
	public static String convertNumberIntl(String contactNumber) {
		if (StringUtils.isEmpty(contactNumber))
			return contactNumber;
		
		contactNumber = convertNumberToLocal(contactNumber);
		return "63" + contactNumber.substring(1);
	}
}
